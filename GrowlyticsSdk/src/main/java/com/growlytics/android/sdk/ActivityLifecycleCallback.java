package com.growlytics.android.sdk;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import androidx.annotation.NonNull;

/**
 * Class for handling activity lifecycle events
 */
final class ActivityLifecycleCallback {

    static boolean registered = false;

    private static synchronized void register(@NonNull Application application, final String growlyticsApiKey) {
        if (Config.getInstance(application.getApplicationContext()).isDisabled()) {
            Logger.info("Growlytics is disabled from config.");
            return;
        }

        if (application == null) {
            Logger.info("Application instance is null/system API is too old");
            return;
        }

        if (registered) {
            Logger.info("Lifecycle callbacks have already been registered");
            return;
        }
        registered = true;

        application.registerActivityLifecycleCallbacks(
                new android.app.Application.ActivityLifecycleCallbacks() {

                    @Override
                    public void onActivityCreated(@NonNull Activity activity, Bundle bundle) {
                        Logger.debug("On Activity Created called");
                        Growlytics instance = Growlytics.getInstance(activity.getApplicationContext());
                        if (instance != null) {
                            instance.onActivityCreated(activity);
                        }
                    }

                    @Override
                    public void onActivityStarted(@NonNull Activity activity) {

                    }

                    @Override
                    public void onActivityResumed(@NonNull Activity activity) {
                        Logger.debug("On Activity Resumed called");
                        Growlytics.getInstance(activity.getApplicationContext()).onActivityResumed(activity);
                        Logger.debug("On Activity Resume finished");
                    }

                    @Override
                    public void onActivityPaused(@NonNull Activity activity) {
                        Growlytics.getInstance(activity.getApplicationContext()).onActivityPaused();
                    }

                    @Override
                    public void onActivityStopped(@NonNull Activity activity) {
                    }

                    @Override
                    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle bundle) {
                    }

                    @Override
                    public void onActivityDestroyed(@NonNull Activity activity) {
                    }
                }
        );
        Logger.info("Application", "Activity Lifecycle Callback successfully registered");
    }

    static synchronized void register(Application application) {
        Logger.info("Application", "Register Activity Lifecycle Callbacks.");
        register(application, null);
    }
}