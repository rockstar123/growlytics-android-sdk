package com.growlytics.android.sdk;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.Iterator;
import java.util.TimeZone;

public class ApiPayloads {

    public static class ApiEndpointNames {
        public static final String RegisterDevice = "create-device";
        public static final String CreateSession = "create-session";
        public static final String CreateEvent = "create-event";
        public static final String Login = "login";
        public static final String Logout = "logout";
        public static final String UpdateCustomer = "update-customer";
        public static final String SaveMobilePushToken = "save-mobile-push-token";
    }

    public interface ApiPayloadInterface {
        String getEventId();

        String getApiEndpointName();

        JSONObject getEventPayload(Context context) throws JSONException;
    }

    public static class ApiPayloadCommon {
        public String apiEndpointId;
        public String apiEndpointName;
        public String apiPayload;
    }

    public static class ApiPayloadBase {
        public JSONObject getMeta() {
            JSONObject meta = new JSONObject();
            try {

                // Device Timestamp, unix epoch in milli seconds.
                meta.put("deviceTs", (int) (System.currentTimeMillis()));

                // Device Timezone Name
//                TimeZone tz = TimeZone.getTimeZone("Australia/Sydney");  // +10 Hours
//                TimeZone tz = TimeZone.getTimeZone("America/Tijuana");  // -8 Hours
                TimeZone tz = TimeZone.getDefault();
                meta.put("deviceTzName", tz.getID());


                // Timezone Offset In Minutes, minutes diff from UTC  from UTC.
                Date now = new Date();
                int hoursDiff = (int) (tz.getOffset(now.getTime()) / 3600000.0 * 60);
                Logger.info("Result: " + hoursDiff);
                meta.put("deviceTz", String.valueOf(hoursDiff));

                // Put Device Timezone Offset.
                // Diff in milliseconds from UTC, Example: +05:30 => 5.5 * 60 * 60 * 1000 =>  19800000
                String milliDiff = String.valueOf(tz.getRawOffset());
                meta.put("deviceTzOffset", milliDiff);

            } catch (JSONException e) {
                Logger.error("ApiPayloadBase", "Failed to create meta object", e);
            }
            return meta;
        }

        protected JSONObject getSessionBody(SessionInfo sessionInfo, Context context) {
            if (sessionInfo == null) {
                return null;
            }

            JSONObject sessionBody = new JSONObject();
            try {

                // Session ID
                sessionBody.put("id", sessionInfo.getSessionId());
                // Session Start Time
                sessionBody.put("startTime", sessionInfo.getCreatedAt());
                // Session Span
                sessionBody.put("sessionSpan", sessionInfo.getSessionLength(context));
                // Session End Time
                sessionBody.put("expiryTime", sessionInfo.getSessionExpiryTime(context));
                // Is First Session
                sessionBody.put("firstSession", sessionInfo.isFirstSession());

            } catch (JSONException e) {
                Logger.error("ApiPayloadBase", "Failed to create sessionBody object", e);
            }
            return sessionBody;
        }
    }

    public static class RegisterDevicePayload extends ApiPayloadBase implements ApiPayloadInterface {

        public String eventId = null;
        public String deviceId;
        public DeviceInfo deviceInfo;

        public RegisterDevicePayload() {

        }

        public String getEventId() {
            if (eventId == null) {
                eventId = CommonUtil.generateGUID();
            }
            return eventId;
        }

        public String getApiEndpointName() {
            return ApiEndpointNames.RegisterDevice;
        }

        public JSONObject getEventPayload(Context context) throws JSONException {
            JSONObject payload = new JSONObject();


            payload.put("id", deviceId);
            payload.put("eventTimestamp", System.currentTimeMillis() / 1000);

            // Put Device Details.
            payload.put("device", deviceInfo.getJSONForServer());

            // Put Meta Info.
            payload.put("meta", getMeta());

            return payload;
        }
    }

    public static class NewSessionPayload extends ApiPayloadBase implements ApiPayloadInterface {

        public String eventId = null;
        public String deviceId;
        public DeviceInfo deviceInfo;
        public SessionInfo sessionInfo;

        public String screenName;

        public NewSessionPayload() {

        }

        public String getEventId() {
            if (eventId == null) {
                eventId = CommonUtil.generateGUID();
            }
            return eventId;
        }

        public String getApiEndpointName() {
            return ApiEndpointNames.CreateSession;
        }

        public JSONObject getEventPayload(Context context) throws JSONException {
            JSONObject payload = new JSONObject();

            payload.put("id", deviceId);

            // Put Session Details.
            JSONObject sessionData = new JSONObject();
            sessionData.put("screenName", screenName);
            payload.put("data", sessionData);


            payload.put("device", deviceInfo.getJSONForServer());
            payload.put("session", getSessionBody(sessionInfo, context));
            payload.put("meta", getMeta());
            payload.put("eventTimestamp", System.currentTimeMillis() / 1000);

            return payload;
        }
    }

    public static class CreateEventApiPayload extends ApiPayloadBase implements ApiPayloadInterface {

        public String eventId = null;
        public String deviceId;
        public DeviceInfo deviceInfo;
        public SessionInfo sessionInfo;
        public JSONObject rawProperties;
        public String eventName;

        public CreateEventApiPayload() {
        }

        public String getEventId() {
            if (eventId == null) {
                eventId = CommonUtil.generateGUID();
            }
            return eventId;
        }

        public String getApiEndpointName() {
            return ApiEndpointNames.CreateEvent;
        }

        public JSONObject getEventPayload(Context context) throws JSONException {

            // Build JSON List With Type from row properties.
            JSONArray parsedPropertyList = new JSONArray();
            Iterator<String> keyListIterator = rawProperties.keys();
            while (keyListIterator.hasNext()) {
                String key = keyListIterator.next();
                Object value = rawProperties.get(key);
                JSONObject attributeInfo = new JSONObject();
                try {
                    attributeInfo.put("name", key);
                    attributeInfo.put("value", value);
                    attributeInfo.put("type", value.getClass().getSimpleName().toLowerCase());
                    parsedPropertyList.put(attributeInfo);
                } catch (JSONException e) {
                    Logger.error("Failed to put attribute into json object. Key: " + key, e);
                }
            }


            JSONObject payload = new JSONObject();
            payload.put("id", deviceId);

            // Put Event Data
            JSONObject eventData = new JSONObject();
            eventData.put("name", eventName);
            eventData.put("rawProperties", rawProperties);
            eventData.put("properties", parsedPropertyList);
            payload.put("event", eventData);

            // Put Other Request Info.
            payload.put("device", deviceInfo.getJSONForServer());
            payload.put("session", getSessionBody(sessionInfo, context));
            payload.put("meta", getMeta());
            payload.put("eventTimestamp", System.currentTimeMillis() / 1000);

            return payload;
        }
    }

    public static class SaveFcmTokenPayload extends ApiPayloadBase implements ApiPayloadInterface {

        // Used For OFfline Storage.
        public String eventId = null;


        public String deviceId;
        public String token;
        public DeviceInfo deviceInfo;
        public SessionInfo sessionInfo;

        @Override
        public String getEventId() {
            if (eventId == null) {
                eventId = CommonUtil.generateGUID();
            }
            return eventId;
        }

        public String getToken() {
            return token;
        }

        public String getApiEndpointName() {
            return ApiEndpointNames.SaveMobilePushToken;
        }

        public JSONObject getEventPayload(Context context) throws JSONException {

            JSONObject payload = new JSONObject();
            payload.put("id", deviceId);

            // Put Event Data
            JSONObject data = new JSONObject();
            data.put("token", token);
            data.put("type", "fcm");
            payload.put("data", data);

            // Put Other Request Info.
            payload.put("device", deviceInfo.getJSONForServer());
            payload.put("session", getSessionBody(sessionInfo, context));
            payload.put("meta", getMeta());
            payload.put("eventTimestamp", System.currentTimeMillis() / 1000);

            return payload;
        }
    }

    public static class LoginUserPayload extends ApiPayloadBase implements ApiPayloadInterface {
        private String eventId = null;

        public String deviceId;

        public JSONArray attributes;
        public String clientCustomerId;
        public DeviceInfo deviceInfo;
        public SessionInfo sessionInfo;

        @Override
        public String getEventId() {
            if (eventId == null) {
                eventId = CommonUtil.generateGUID();
            }
            return eventId;
        }

        @Override
        public String getApiEndpointName() {
            return ApiEndpointNames.Login;
        }

        @Override
        public JSONObject getEventPayload(Context context) throws JSONException {

            // Put Event Data
            JSONObject eventData = new JSONObject();
            eventData.put("id", this.deviceId);

            // Put User Data
            JSONObject data = new JSONObject();
            data.put("id", this.clientCustomerId);
            data.put("attributes", this.attributes);
            eventData.put("user", data);

            eventData.put("device", this.deviceInfo.getJSONForServer());
            eventData.put("session", getSessionBody(this.sessionInfo, context));
            eventData.put("meta", getMeta());
            eventData.put("eventTimestamp", System.currentTimeMillis() / 1000);

            return eventData;
        }
    }

    public static class LogoutUserPayload extends ApiPayloadBase implements ApiPayloadInterface {
        public String eventId = null;

        public String deviceId;
        public String clientCustomerId;
        public SessionInfo sessionInfo;
        public DeviceInfo deviceInfo;

        @Override
        public String getEventId() {
            if (eventId == null) {
                eventId = CommonUtil.generateGUID();
            }
            return eventId;
        }

        @Override
        public String getApiEndpointName() {
            return ApiEndpointNames.Logout;
        }

        @Override
        public JSONObject getEventPayload(Context context) throws JSONException {

            // Put Event Data
            JSONObject eventData = new JSONObject();

            eventData.put("id", this.deviceId);
            eventData.put("sid", this.clientCustomerId);
            eventData.put("device", this.deviceInfo.getJSONForServer());
            eventData.put("session", getSessionBody(this.sessionInfo, context));
            eventData.put("meta", getMeta());
            eventData.put("eventTimestamp", System.currentTimeMillis() / 1000);

            return eventData;
        }
    }
}
