package com.growlytics.android.sdk;

import android.content.Context;
import android.os.RemoteException;

import androidx.annotation.RestrictTo;

import com.android.installreferrer.api.InstallReferrerClient;
import com.android.installreferrer.api.InstallReferrerStateListener;
import com.android.installreferrer.api.ReferrerDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RestrictTo(RestrictTo.Scope.LIBRARY)
public class AppLaunchedHandler {

    private static AppLaunchedHandler instance = null;

    private final ExecutorService es;
    private final Context context;
    private Config config;
    private final EventQueueService eventQueue;
    DeviceInfo deviceInfo;

    // Sqlite Helper & Syncing

    private final Object appLaunchPushedLock = new Object();
    private static boolean appLaunchPushed = false;

    private AppLaunchedHandler(final Context context) {
        this.context = context.getApplicationContext();
        this.config = Config.getInstance(context);
        this.es = Executors.newFixedThreadPool(1);
        this.eventQueue = EventQueueService.getInstance(context);
        this.deviceInfo = new DeviceInfo(context);
    }

    static AppLaunchedHandler getInstance(Context context) {
        if (instance != null) {
            return instance;
        }

        // Initialize instance.
        Logger.info("AppLaunchedHandler Instance is initializing.");
        instance = new AppLaunchedHandler(context.getApplicationContext());
        return instance;
    }

    void handleAppLaunched(String screenName, TrafficSourceInfo trafficSourceInfo) {
        String logPrefix = "AppLaunch:";

        synchronized (appLaunchPushedLock) {
            try {

                // Check & Sync Device Id.
                String deviceId = DeviceService.getDeviceId(context);
                if (deviceId == null) {
                    Logger.info(logPrefix, "Device id not found, generate & sync device id.");
                    deviceId = DeviceService.generateAndSyncDeviceId(this.context, this.deviceInfo);
                    // Track app install for new device id.
                    Logger.info(logPrefix, "Check app install for new device id.");
                    trackAppInstalledEvent(deviceId);
                }

                // clear expired events from db
                eventQueue.removeExpiredEventsFromDb();

                // Check if app launch event is already pushed.
                SessionService sessionService = new SessionService(this.context);
                if (!sessionService.isSessionExpired() && AppLaunchedHandler.appLaunchPushed) {
                    Logger.info(logPrefix, "SessionInfo Exists & App Launched has already been triggered. Will not trigger it.");
                    return;
                }
                Logger.info(logPrefix, "Session does not exist or expired, will trigger app launch event.");

                // Create Session If Expired.
                if (sessionService.isSessionExpired()) {
                    Logger.info(logPrefix, "Session is expired. Creating new session.");
                    sessionService.createSessionIfExpired(screenName, this.deviceInfo);

                    // Create App Launch Event.
                    try {
                        Logger.info(logPrefix, "Create App Launch Event With New Session.");
                        ApiPayloads.CreateEventApiPayload appLaunchPayload = new ApiPayloads.CreateEventApiPayload();
                        appLaunchPayload.deviceId = deviceId;
                        appLaunchPayload.deviceInfo = this.deviceInfo;
                        appLaunchPayload.eventName = Constants.SdkEventNames.APP_LAUNCH;
                        appLaunchPayload.sessionInfo = sessionService.getSessionInfo();
                        appLaunchPayload.rawProperties = _getAppLaunchedFields(trafficSourceInfo);

                        EventQueueService.getInstance(context).addEventToQueue(appLaunchPayload);
                    } catch (Throwable t) {
                        // We won't get here
                        Logger.error("Failed to construct App Launched event fields", t);
                    }

                } else {
                    Logger.debug(logPrefix, "Session is not expired. Will not create new session.");
                }
                AppLaunchedHandler.appLaunchPushed = true;

            } catch (Throwable t) {
                Logger.error("Failed to handle App Launched", t);
            }
        }
    }

    void trackAppInstalledEvent(String deviceId) {
        // App install tracking
        _postAsyncSafely("queueEvent", () -> {
            Logger.info("App Install:", "Request received.");

            InstallReferrerClient referrerClient = InstallReferrerClient.newBuilder(context).build();
            referrerClient.startConnection(new InstallReferrerStateListener() {
                @Override
                public void onInstallReferrerSetupFinished(int responseCode) {
                    switch (responseCode) {
                        case InstallReferrerClient.InstallReferrerResponse.OK:
                            // Connection established.
                            ReferrerDetails response;
                            try {
                                response = referrerClient.getInstallReferrer();
                                String referrerUrl = response.getInstallReferrer();
                                long referrerClickTime = response.getReferrerClickTimestampSeconds();
                                long appInstallTime = response.getInstallBeginTimestampSeconds();
                                boolean instantExperienceLaunched = response.getGooglePlayInstantParam();
                                String installedVersion = response.getInstallVersion();
                                referrerClient.endConnection();

                                JSONObject rawProperties = new JSONObject();
                                rawProperties.put("referrerUrl", referrerUrl);
                                rawProperties.put("referrerClickTime", referrerClickTime);
                                rawProperties.put("appInstallTime", appInstallTime);
                                rawProperties.put("instantExperienceLaunched", instantExperienceLaunched);

                                ApiPayloads.CreateEventApiPayload appLaunchPayload = new ApiPayloads.CreateEventApiPayload();
                                appLaunchPayload.deviceId = deviceId;
                                appLaunchPayload.deviceInfo = deviceInfo;
                                appLaunchPayload.eventName = Constants.SdkEventNames.APP_INSTALLED;
                                appLaunchPayload.sessionInfo = null;
                                appLaunchPayload.rawProperties = rawProperties;

                                EventQueueService.getInstance(context).addEventToQueue(appLaunchPayload);
                                Logger.info("App Install", "Referrer Url " + referrerUrl + " " + installedVersion);
                            } catch (RemoteException | JSONException e) {
                                Logger.error("App Install", "RemoteException " + e);
                            }

                            break;
                        case InstallReferrerClient.InstallReferrerResponse.FEATURE_NOT_SUPPORTED:
                            // API not available on the current Play Store app.
                            break;
                        case InstallReferrerClient.InstallReferrerResponse.SERVICE_UNAVAILABLE:
                            // Connection couldn't be established.
                            break;
                    }
                }

                @Override
                public void onInstallReferrerServiceDisconnected() {
                    // Try to restart the connection on the next request to
                    // Google Play by calling the startConnection() method.
                    trackAppInstalledEvent(deviceId);
                }
            });
        });
    }

    private JSONObject _getAppLaunchedFields(TrafficSourceInfo trafficSourceInfo) throws Exception {
        try {

            JSONObject evtData = new JSONObject();
            evtData.put("firstTime", false);

            // Put traffic source info
//            if (trafficSourceInfo != null) {
//                evtData.put("utmInfo", trafficSourceInfo.getUtmInfo());
//                evtData.put("referrer", trafficSourceInfo.getReferrer());
//            }


            // Put Location info
            // if (locationFromUser != null) {
            // evtData.put("Latitude", locationFromUser.getLatitude());
            // evtData.put("Longitude", locationFromUser.getLongitude());
            // }

            // send up googleAdID
            // if (this.deviceInfo.getGoogleAdID() != null) {
            //  String baseAdIDKey = "GoogleAdID";
            //String adIDKey = deviceIsMultiUser() ? Constants.MULTI_USER_PREFIX + baseAdIDKey : baseAdIDKey;
            //evtData.put(adIDKey, this.deviceInfo.getGoogleAdID());
            //evtData.put("GoogleAdIDLimit", this.deviceInfo.isLimitAdTrackingEnabled());
            //}

            return evtData;
        } catch (Throwable t) {
            Logger.info("Failed to construct App Launched event");
            throw t;
        }
    }

    private void _postAsyncSafely(final String name, final Runnable runnable) {
        try {
            // Original Logic
//            final boolean executeSync = Thread.currentThread().getId() == EXECUTOR_THREAD_ID;
//
//            if (executeSync) {
//                runnable.run();
//            } else {
//                es.submit(new Runnable() {
//                    @Override
//                    public void run() {
//                        EXECUTOR_THREAD_ID = Thread.currentThread().getId();
//                        try {
//                            runnable.run();
//                        } catch (Throwable t) {
//                            Logger.info("Executor service: Failed to complete the scheduled task", t);
//                        }
//                    }
//                });
//            }
            es.execute(runnable);
        } catch (Throwable t) {
            Logger.info("Failed to submit task to the executor service", t);
        }
    }

}
