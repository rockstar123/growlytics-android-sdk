package com.growlytics.android.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import androidx.annotation.NonNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.UUID;

import javax.net.ssl.HttpsURLConnection;

class CommonUtil {

    @NonNull
    static String generateGUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    static boolean isNetworkOnline(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm == null) {
                // lets be optimistic, if we are truly offline we handle the exception
                return true;
            }
            @SuppressLint("MissingPermission")
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnected();

        } catch (Throwable e) {
            Logger.info("Network-Check", "Failed to check network availablity.", e);
            // lets be optimistic, if we are truly offline we handle the exception
            return true;
        }

    }

    static byte[] getByteArrayFromImageURL(String srcUrl) {
        srcUrl = srcUrl.replace("///", "/");
        srcUrl = srcUrl.replace("//", "/");
        srcUrl = srcUrl.replace("http:/", "http://");
        srcUrl = srcUrl.replace("https:/", "https://");
        HttpsURLConnection connection = null;
        try {
            URL url = new URL(srcUrl);
            connection = (HttpsURLConnection) url.openConnection();
            InputStream is = connection.getInputStream();
            byte[] buffer = new byte[8192];
            int bytesRead;
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            while ((bytesRead = is.read(buffer)) != -1) {
                baos.write(buffer, 0, bytesRead);
            }
            return baos.toByteArray();
        } catch (IOException e) {
            Logger.debug("CommonUtil", "Error processing image bytes from url: " + srcUrl);
            return null;
        } finally {
            try {
                if (connection != null) {
                    connection.disconnect();
                }
            } catch (Throwable t) {
                Logger.debug("CommonUtil", "Couldn't close connection!", t);
            }
        }
    }


    static Bitmap getBitmapFromURL(String srcUrl, int timeoutInSeconds) {
        HttpURLConnection connection = null;
        try {
            URL url = new URL(srcUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(timeoutInSeconds * 1000);
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            Logger.error("GrwNotification", "Couldn't download the notification icon. URL was: " + srcUrl);
            return null;
        } finally {
            try {
                if (connection != null) {
                    connection.disconnect();
                }
            } catch (Throwable t) {
                Logger.error("GrwNotification", "Couldn't close connection!", t);
            }
        }


    }

    static Bitmap getNotificationBitmap(String icoPath, boolean fallbackToAppIcon, final Context context)
            throws NullPointerException {

        // If the icon path is not specified
//        if (icoPath == null || icoPath.equals("")) {
        return fallbackToAppIcon ? _getAppIcon(context) : null;
//        }

//         Simply stream the bitmap
//        if (!icoPath.startsWith("http")) {
//            icoPath = Constants.ICON_BASE_URL + "/" + icoPath;
//        }

//        Bitmap ic = getBitmapFromURL(icoPath);
//        return (ic != null) ? ic : ((fallbackToAppIcon) ? getAppIcon(context) : null);
    }

    private static Bitmap _getAppIcon(final Context context) throws NullPointerException {
        // Try to get the app logo first
        try {
            Drawable logo = context.getPackageManager().getApplicationLogo(context.getApplicationInfo());
            if (logo == null)
                throw new Exception("Logo is null");
            return drawableToBitmap(logo);
        } catch (Exception e) {
            // Try to get the app icon now
            // No error handling here - handle upstream
            return drawableToBitmap(context.getPackageManager().getApplicationIcon(context.getApplicationInfo()));
        }
    }

    static Bitmap drawableToBitmap(Drawable drawable)
            throws NullPointerException {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    static String getHaxForBackgroundColor(String hax, int transparency){
        HashMap<Integer, String> map = new HashMap<Integer, String>();
        map.put(100,"FF");
        map.put(95,"F2");
        map.put(90,"E6");
        map.put(85,"D9");
        map.put(80,"CC");
        map.put(75,"BF");
        map.put(70,"B3");
        map.put(65,"A6");
        map.put(60,"99");
        map.put(55,"8C");
        map.put(50,"80");
        map.put(45,"73");
        map.put(40,"66");
        map.put(35,"59");
        map.put(30,"4D");
        map.put(25,"40");
        map.put(20,"33");
        map.put(15,"26");
        map.put(10,"1A");
        map.put(5,"0D");
        map.put(0,"00");

        int nearest = transparency + (( 5 - (transparency % 5)) % 5);
        Logger.info("Nearest is " + nearest);
        if(!map.containsKey(nearest)){
            return hax;
        }else{
            return "#" + map.get(nearest) + hax.substring(1);
        }
    }

}
