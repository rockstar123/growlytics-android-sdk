package com.growlytics.android.sdk;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

class Config {

    private final String apiKey;
    private static Config instance = null;
    private Boolean isDisabled = false;
    private Boolean debug = false;
    private String host = "https://dc.growlytics.in";
    private String fcmSenderId;
    private final String notificationIcon;
    private int sessionLength = 30;

    synchronized static Config getInstance(Context context) {
        if (instance == null) {
            instance = new Config(context);
        }
        return instance;
    }

    String getApiKey() {
        return this.apiKey;
    }

    boolean isDisabled() {
        return isDisabled;
    }

    void setDisabled(boolean disabled) {
        isDisabled = disabled;
    }

    String getNotificationIcon() {
        return notificationIcon;
    }

    Boolean getDebug() {
        return this.debug;
    }

    String getHost() {
        return this.host;
    }

    String getFcmSenderId() {
        return this.fcmSenderId;
    }

    int getSessionLength() {
        return this.sessionLength;
    }

    private Config(Context context) {

        Bundle metaData = null;
        try {
            PackageManager pm = context.getPackageManager();
            ApplicationInfo ai = pm.getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            metaData = ai.metaData;
        } catch (Throwable t) {
            // no-op
        }

        if (metaData == null) {
            metaData = new Bundle();
        }

        apiKey = _getManifestStringValueForKey(metaData, Constants.MANIFEST_API_KEY);
        isDisabled = "1".equals(_getManifestStringValueForKey(metaData, Constants.MANIFEST_DISABLED));
        notificationIcon = _getManifestStringValueForKey(metaData, Constants.MANIFEST_NOTIFICATION_ICON);

        // Set debug info
        debug = "1".equals(_getManifestStringValueForKey(metaData, Constants.MANIFEST_DEBUG));
        if (!debug) {
            Logger.setEnabled(true);
        }

        fcmSenderId = _getManifestStringValueForKey(metaData, Constants.LABEL_FCM_SENDER_ID);
        if (fcmSenderId != null) {
            fcmSenderId = fcmSenderId.replace("id:", "");
        }

        String inputHost = _getManifestStringValueForKey(metaData, Constants.MANIFEST_HOST);
        if (inputHost != null) {
            host = inputHost;
        }

        // Read Session Length
        String sessionLengthInConfig = _getManifestStringValueForKey(metaData, Constants.MANIFEST_SESSION_LENGTH);
        if (sessionLengthInConfig != null) {
            sessionLength = Integer.parseInt(sessionLengthInConfig);
        }
    }

    private static String _getManifestStringValueForKey(Bundle manifest, String name) {
        try {
            Object o = manifest.get(name);
            return (o != null) ? o.toString() : null;
        } catch (Throwable t) {
            return null;
        }
    }
}
