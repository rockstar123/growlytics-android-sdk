package com.growlytics.android.sdk;

class Constants {

    // Db & Storage constants
    static final String SHARED_PREF_STORAGE_TAG = "Growlytics";

    static class SdkEventNames {
        static final String APP_INSTALLED = "GRW_SYSTEM_EVENT_APP_INSTALL";
        static final String APP_LAUNCH = "GRW_SYSTEM_EVENT_APP_LAUNCH";
    }

    // EVENT STORAGE EXPIRY TIME
    static final int STORAGE_EVENT_EXPIRE_AFTER = 60 * 60 * 24 * 3;

    // Manifest Constants
    static final String MANIFEST_API_KEY = "GROWLYTICS_API_KEY";
    static final String MANIFEST_DISABLED = "GROWLYTICS_DISABLED";
    static final String MANIFEST_DEBUG = "GROWLYTICS_DEBUG";
    static final String MANIFEST_HOST = "GROWLYTICS_HOST";
    static final String MANIFEST_SESSION_LENGTH = "GROWLYTICS_SESSION_LENGTH";
    static final String MANIFEST_NOTIFICATION_ICON = "GROWLYTICS_NOTIFICATION_ICON";

    // Event validation constants
    static final int MAX_KEY_LENGTH = 120;
    static final int MAX_VALUE_LENGTH = 512;
    static final int MAX_CUSTOMER_ID_LENGTH = 40;

    // Engagement Constants
    static final String LABEL_FCM_SENDER_ID = "FCM_SENDER_ID";

    // GrwNotification Constants
    static final int NOTIFICATION_DEFAULT_TTL = 1000 * 60 * 60 * 24 * 4;
    static final String NOTIFICATION_ACTION_BTNS = "glytcs_btns";
    static final String NOTIFICATION_PRIORITY_HIGH = "high";
    static final String NOTIFICATION_PRIORITY_MAX = "max";
    static final String NOTIFICATION_TTL = "wzrk_ttl";
    static final String NOTIFICATION_CLICK_CAPTURED = "glytcs_clk_done";
}
