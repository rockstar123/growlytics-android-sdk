package com.growlytics.android.sdk;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;

import org.json.JSONException;

import java.io.File;


enum DbSettingKey {
    DeviceId,
    SessionInfo,

    LoggedInUserId,

    FcmTokenReportedTimestamp
}

class DbStorageService {

    private final Context context;
    private static DbStorageService instance;

    private static final String DATABASE_NAME = "growlytics";
    private static final int DATABASE_VERSION = 3;

    // Tables
    private static final String infoTableName = "commonInfo";
    private static final String eventTableName = "event";
    private final SQLiteDb sqLiteDb;

    private final Object sessionWriteLock = true;

    // ************************** Start: Singleton Class Methods *******************************/

    private DbStorageService(final Context context) {
        this.context = context;
        this.sqLiteDb = new SQLiteDb(context, DATABASE_NAME);
    }

    static DbStorageService getInstance(Context context) {
        if (instance != null) {
            return instance;
        }

        // Initialize instance.
        instance = new DbStorageService(context);
        return instance;
    }

    // ************************** End: Singleton Class Methods *******************************/

    // ************************** Start: Settings Table *******************************/

    static String getSetting(Context context, DbSettingKey key){
        try {
            String query = "select * from " + infoTableName + " where label = '"+key+"'";
            Cursor cursor =  getInstance(context).sqLiteDb.getReadableDatabase().rawQuery(query, null);

            String value = null;
            if (cursor.moveToFirst()) {
                value = cursor.getString(cursor.getColumnIndex("value"));
            }
            cursor.close();

            return value;

        } catch (final Exception e) {
            Logger.info("SqliteHelper", "Failed to read session info.", e);
            throw e;
        }
    }

    static void saveSetting(Context context, DbSettingKey key, String value){
        Logger.debug("SqliteHelper", "Storing setting in db." + key + value);

        //check for memory overflow
        if (!getInstance(context).sqLiteDb.belowMemThreshold()) {
            Logger.warn("There is not enough space left on the device to store data, data discarded");
            return;
        }

        try {
            final SQLiteDatabase db = getInstance(context).sqLiteDb.getWritableDatabase();
            final ContentValues cv = new ContentValues();
            cv.put("label", key.toString());
            cv.put("value", value);
            cv.put("created_at", System.currentTimeMillis());

            // Delete existing
            String query = "delete from " + infoTableName + " where label = '" + key + "'";
            db.execSQL(query);

            // Insert new session info
            db.insert(infoTableName, null, cv);
            Logger.debug("SqliteHelper", "Session stored in db.");

        } catch (final SQLiteException e) {
            Logger.error("SqliteHelper", "Failed to save setting: " + key + " Recreating DB", e);
        }
    }

    static void removeSetting(Context context, DbSettingKey key) {
        try {
            SQLiteDatabase db = getInstance(context).sqLiteDb.getWritableDatabase();

            String query = "delete from " + infoTableName + " where label = '" + key + "'";
            db.execSQL(query);

        } catch (SQLiteException e) {
            Logger.error("SqliteHelper", "Failed to delete Setting In Table: " + key, e);
        }
    }


    // ************************** End: Settings Table *******************************/

    // ************************** Start: Event Queue Table *******************************/

    private final Object eventWriteLock = true;

    void queueEventToDb(ApiPayloads.ApiPayloadInterface event) {

        synchronized (eventWriteLock) {

            Logger.debug("SqliteHelper", "Storing event in db: " + event.getApiEndpointName());

            //check for memory overflow
            if (!this.sqLiteDb.belowMemThreshold()) {
                Logger.warn("There is not enough space left on the device to store data, data discarded");
                return;
            }

            try {
                final SQLiteDatabase db = sqLiteDb.getWritableDatabase();
                final ContentValues cv = new ContentValues();
                cv.put("data", event.getEventPayload(this.context).toString());
                cv.put("_id", event.getEventId());
                cv.put("name", event.getApiEndpointName());
                cv.put("created_at", System.currentTimeMillis());

                db.insert(eventTableName, null, cv);
                Logger.debug("SqliteHelper", "Event stored in db.");

            } catch (final JSONException e) {
                Logger.info("SqliteHelper", "Failed to serialize event to store it in db. " + eventTableName + " Recreating DB");
            } catch (final SQLiteException e) {
                Logger.info("SqliteHelper", "Error adding data to event table " + eventTableName + " Recreating DB");
                sqLiteDb.deleteDatabase();
            }
        }
    }

    ApiPayloads.ApiPayloadCommon getAnyQueuedEvent() throws Exception {

        synchronized (eventWriteLock) {
            try {

                Cursor cursor = sqLiteDb.getReadableDatabase().rawQuery("select * from " + eventTableName + " limit 1", null);
                if (cursor.moveToFirst()) {
                    String eventId = cursor.getString(cursor.getColumnIndex("_id"));
                    String eventName = cursor.getString(cursor.getColumnIndex("name"));
                    String eventRawData = cursor.getString(cursor.getColumnIndex("data"));


                    ApiPayloads.ApiPayloadCommon payload = new ApiPayloads.ApiPayloadCommon();
                    payload.apiEndpointName = eventName;
                    payload.apiEndpointId = eventId;
                    payload.apiPayload = eventRawData;

                    cursor.close();
                    return payload;
                }
                return null;

            } catch (final SQLiteException e) {
                Logger.info("SqliteHelper", "Failed to read event list.", e);
                throw e;
            } catch (final Exception e) {
                Logger.info("SqliteHelper", "Failed to read event list.", e);
                throw e;
            }
        }
    }

    void removeEventFromDb(String eventId) {
        synchronized (eventWriteLock) {

            Logger.debug("SqliteHelper", "Removing event from db. id:" + eventId);

            try {
                String query = "Delete from " + eventTableName + " where _id = '" + eventId + "'";
                sqLiteDb.getWritableDatabase().execSQL(query);
                Logger.debug("SqliteHelper", "Event removed from db. id: " + eventId);

            } catch (final SQLiteException e) {
                Logger.error("SqliteHelper", "Failed to remove event from db. Recreating DB");
                sqLiteDb.deleteDatabase();
            }
        }
    }

    void removeExpiredEventsFromDb() {
        synchronized (eventWriteLock) {

            Logger.debug("SqliteHelper", "Removing expired events from db.");

            try {
                int timestampToCompare = (int) (System.currentTimeMillis() / 1000) - Constants.STORAGE_EVENT_EXPIRE_AFTER;
                String query = "Delete from " + eventTableName + " where created_at <= " + timestampToCompare;

                sqLiteDb.getWritableDatabase().execSQL(query);
                Logger.debug("SqliteHelper", "Removed expired events from db.");

            } catch (final SQLiteException e) {
                Logger.error("SqliteHelper", "Failed to remove expired events from db. Recreating DB", e);
                sqLiteDb.deleteDatabase();
            }
        }
    }

    void clearEventQueue() {
        synchronized (eventWriteLock) {
            Logger.debug("SqliteHelper", "Clearing event queue.");
            try {
                final SQLiteDatabase db = sqLiteDb.getWritableDatabase();
                db.delete(eventTableName, null, null);
                Logger.debug("SqliteHelper", "Event queue cleared.");
            } catch (final SQLiteException e) {
                Logger.debug("Network", "Error removing all events from table " + eventTableName + " Recreating DB", e);
                sqLiteDb.deleteDatabase();
            }
        }
    }

    // ************************** End: Event Queue Table *******************************/

    private static class SQLiteDb extends SQLiteOpenHelper {

        private static final String CREATE_SESSION_TABLE =
                "CREATE TABLE " + infoTableName + " (_id INTEGER PRIMARY KEY , " +
                        "label STRING NOT NULL, " +
                        "value STRING NOT NULL, " +
                        "created_at INTEGER NOT NULL);";

        private static final String CREATE_EVENTS_TABLE =
                "CREATE TABLE " + eventTableName + " (_id STRING PRIMARY KEY, " +
                        "name STRING NOT NULL, " +
                        "data STRING NOT NULL, " +
                        "created_at INTEGER NOT NULL);";


        SQLiteDb(Context context, String dbName) {
            super(context, dbName, null, DATABASE_VERSION);
            databaseFile = context.getDatabasePath(dbName);
        }

        void deleteDatabase() {
            close();
            //noinspection ResultOfMethodCallIgnored
            databaseFile.delete();
        }

        @SuppressLint("SQLiteString")
        @Override
        public void onCreate(@NonNull SQLiteDatabase db) {
            Logger.info("Syncing Growlytics Db");
            db.execSQL(CREATE_SESSION_TABLE);
            db.execSQL(CREATE_EVENTS_TABLE);
            Logger.info("Synced Growlytics Db");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Logger.info("Upgrading Growlytics DB to version " + newVersion);
        }

        boolean belowMemThreshold() {
            //noinspection SimplifiableIfStatement
            if (databaseFile.exists()) {
                return Math.max(databaseFile.getUsableSpace(), DB_LIMIT) >= databaseFile.length();
            }
            return true;
        }

        private final File databaseFile;

        private final int DB_LIMIT = 20 * 1024 * 1024; //20mb
    }

}
