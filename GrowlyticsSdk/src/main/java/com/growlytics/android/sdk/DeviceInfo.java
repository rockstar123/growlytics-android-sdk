package com.growlytics.android.sdk;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.telephony.TelephonyManager;

import androidx.core.app.NotificationManagerCompat;

import org.json.JSONObject;

import java.util.Locale;

class DeviceInfo {

    private Context context;
    private static DeviceCachedInfo cachedInfo;

    public DeviceInfo(Context context) {
        this.context = context;
        getDeviceCachedInfo(context);
    }

    private DeviceCachedInfo getDeviceCachedInfo(Context context) {
        if (cachedInfo == null) {
            cachedInfo = new DeviceCachedInfo(context);
        }
        return cachedInfo;
    }

    public String getLanguage() {
        return getDeviceCachedInfo(context).language;
    }

    public String getDeviceType() {
        return getDeviceCachedInfo(context).deviceType;
    }

    public String getAppVersionName() {
        return getDeviceCachedInfo(context).appVersionName;
    }

    public int getAppVersionCode() {
        return getDeviceCachedInfo(context).appVersionCode;
    }

    public String getOsName() {
        return getDeviceCachedInfo(context).osName;
    }

    public String getOsVersion() {
        return getDeviceCachedInfo(context).osVersion;
    }

    public String getManufacturer() {
        return getDeviceCachedInfo(context).manufacturer;
    }

    public String getModel() {
        return getDeviceCachedInfo(context).model;
    }

    public String getCarrier() {
        return getDeviceCachedInfo(context).carrier;
    }

    public String getCountryCode() {
        return getDeviceCachedInfo(context).countryCode;
    }

    public int getSdkVersion() {
        return getDeviceCachedInfo(context).sdkVersion;
    }

    String getFCMSenderID() {
        return Config.getInstance(context).getFcmSenderId();
    }

    public boolean getNotificationsEnabledForUser() {
        return getDeviceCachedInfo(context).notificationsEnabled;
    }

    static int getAppIconAsIntId(final Context context) {
        ApplicationInfo ai = context.getApplicationInfo();
        return ai.icon;
    }


    JSONObject getJSONForServer() {

        JSONObject evtData = new JSONObject();
        try {

            evtData.put("appVersionCode", this.getAppVersionCode());
            evtData.put("appVersionName", this.getAppVersionName());
            evtData.put("sdkVersion", this.getSdkVersion());

            evtData.put("deviceType", this.getDeviceType());
            evtData.put("deviceManufacturer", this.getManufacturer());
            evtData.put("deviceModel", this.getModel());
            evtData.put("deviceCarrier", this.getCarrier());

            evtData.put("osName", this.getOsName());
            evtData.put("osVersion", this.getOsVersion());


            evtData.put("language", this.getLanguage());
            evtData.put("platform", "ANDROID");

        } catch (Throwable t) {
            // Ignore
            Logger.error("DeviceInfo", "Error while creating JSON for server.", t);
        }

        return evtData;
    }

    private class DeviceCachedInfo {

        private String deviceType;
        private int appVersionCode;
        private String appVersionName;
        private String osName;
        private String osVersion;
        private String language;
        private String manufacturer;
        private String model;
        private String carrier;
        private String countryCode;
        private int sdkVersion;
        private boolean notificationsEnabled;

        DeviceCachedInfo(Context context) {
            deviceType = _getDeviceType();
            appVersionName = _getAppVersionName();
            appVersionCode = _getAppVersionCode();
            osName = _getOsName();
            osVersion = _getOsVersion();
            manufacturer = _getManufacturer();
            model = _getModel();
            carrier = _getCarrier();
            language = Locale.getDefault().getLanguage();

            countryCode = _getCountryCode();
            sdkVersion = _getSdkVersion();
            notificationsEnabled = _getNotificationEnabledForUser();
        }

        private String _getDeviceType() {
            boolean isTablet = (context.getResources().getConfiguration().screenLayout
                    & Configuration.SCREENLAYOUT_SIZE_MASK)
                    >= Configuration.SCREENLAYOUT_SIZE_LARGE;

            if (isTablet) return "TABLET";
            else return "MOBILE";
        }

        private String _getAppVersionName() {
            PackageInfo packageInfo;
            try {
                packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                return packageInfo.versionName;
            } catch (PackageManager.NameNotFoundException e) {
                Logger.info("Unable to get app version");
            }
            return null;
        }

        private String _getOsName() {
            return "Android";
        }

        private String _getOsVersion() {
            return this._getOsName() + " " + Build.VERSION.RELEASE;
        }

        private String _getManufacturer() {
            return Build.MANUFACTURER;
        }

        private String _getModel() {
            return Build.MODEL;
        }

        private String _getCarrier() {
            try {
                String operator = null;
                TelephonyManager manager = (TelephonyManager) context
                        .getSystemService(Context.TELEPHONY_SERVICE);
                if (manager != null) {
                    operator = manager.getNetworkOperatorName();
                }
                if (operator == null || operator.isEmpty()) {
                    return "Unknown";
                }
                return operator;
            } catch (Exception e) {
                // Failed to get network operator name from network
                return "Unknown";
            }
        }

        private int _getAppVersionCode() {
            PackageInfo packageInfo;
            try {
                packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                return packageInfo.versionCode;
            } catch (PackageManager.NameNotFoundException e) {
                Logger.info("Unable to get app appVersionCode");
            }
            return 0;
        }

        private String _getCountryCode() {
            try {
                TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                if (tm != null) {
                    return tm.getSimCountryIso();
                }
            } catch (Throwable ignore) {
                return "";
            }
            return "";
        }

        private int _getSdkVersion() {
            return (int) BuildConfig.VERSION_CODE;
            //BuildConfig.VERSION_CODE;
        }

        private boolean _getNotificationEnabledForUser() {
            return NotificationManagerCompat.from(context).areNotificationsEnabled();
        }
    }
}
