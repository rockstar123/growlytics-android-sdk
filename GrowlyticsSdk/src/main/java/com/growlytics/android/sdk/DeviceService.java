package com.growlytics.android.sdk;

import android.content.Context;

public class DeviceService {
    private final static Object appLaunchPushedLock = new Object();


    public static String getDeviceId(Context context){
        String setting = DbStorageService.getSetting(context, DbSettingKey.DeviceId);
        if(setting == null) {
            return null;
        }

        return setting;
    }

    public static void saveDeviceId(Context context, String deviceId){
        DbStorageService.saveSetting(context,DbSettingKey.DeviceId, deviceId);
    }

    public static String generateAndSyncDeviceId(Context context, DeviceInfo deviceInfo) {
        synchronized (appLaunchPushedLock) {
            String deviceId;
            // If device id not found, generate & Register Device Id.
            deviceId = "GRW_" + CommonUtil.generateGUID();

            // Report New Device To Server.
            Logger.info("logPrefix", "Report new Device ID: " + deviceId);
            ApiPayloads.RegisterDevicePayload registerDevicePayload = new ApiPayloads.RegisterDevicePayload();
            registerDevicePayload.deviceId = deviceId;
            registerDevicePayload.deviceInfo = deviceInfo;
            EventQueueService.getInstance(context).addEventToQueue(registerDevicePayload);

            // Save Device In Db.
            Logger.info("logPrefix", "Save New Device ID in Db: " + deviceId);
            saveDeviceId(context, deviceId);

            return deviceId;
        }
    }
}
