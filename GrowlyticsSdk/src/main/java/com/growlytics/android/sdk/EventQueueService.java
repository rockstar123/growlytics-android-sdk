package com.growlytics.android.sdk;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


import static com.growlytics.android.sdk.CommonUtil.isNetworkOnline;

public class EventQueueService {

    private ExecutorService es;

    DeviceInfo deviceInfo = null;

    private static EventQueueService instance = null;
    private Context context;
    private Config config = null;


    // Sqlite Helper & Syncing
    private DbStorageService dbStorage;
    private Handler handlerUsingMainLooper;

    private final Object queueFlushSyncronizeLock = new Object();

    private EventQueueService(final Context context) {
        this.context = context;
        this.config = Config.getInstance(context);
        this.es = Executors.newFixedThreadPool(2);
        this.dbStorage = DbStorageService.getInstance(context);
        this.deviceInfo = new DeviceInfo(context);
        this.handlerUsingMainLooper = new Handler(Looper.getMainLooper());
    }

    static EventQueueService getInstance(Context context) {
        if (instance != null) {
            return instance;
        }

        // Initialize instance.
        instance = new EventQueueService(context);
        return instance;
    }

    void addEventToQueue(final ApiPayloads.ApiPayloadInterface eventPayload) {

        try {
            Logger.debug("Event Queue", "Processing event: " + eventPayload.getApiEndpointName());
            dbStorage.queueEventToDb(eventPayload);
            _scheduleQueueFlush(0);
        } catch (Throwable e) {
            Logger.info("Event Processing", "Failed to queue event: " + eventPayload.getApiEndpointName(), e);
        }
    }


    private Runnable _eventQueueRunnable;
    private void _scheduleQueueFlush(int seconds) {
        Logger.debug("EventProcessor", "Scheduling queue flush.");
        if (_eventQueueRunnable == null) {
            _eventQueueRunnable = this::_flushQueueAsync;
        }
        handlerUsingMainLooper.removeCallbacks(_eventQueueRunnable);
        handlerUsingMainLooper.postDelayed(_eventQueueRunnable, seconds * 1000L);

        // Re-issue a new delayed(one second) runnable
        Logger.debug("EventProcessor", "Queue flush scheduled.");
    }

    private void _flushQueueAsync() {
        synchronized (queueFlushSyncronizeLock) {
            _postAsyncSafely("EventProcessor#flushQueueAsync", () -> {
                // One flush at a time
                try {

                    Logger.debug("EventProcessor-DbFlush", "Executing db flush.");

                    if (!isNetworkOnline(context)) {
                        Logger.debug("EventProcessor-DbFlush", "Network connectivity unavailable. Will retry after 2 minutes.");
                        _scheduleQueueFlush(60 * 2);
                        return;
                    }

                    // Send all the events one by one on http, at the same time
                    ApiPayloads.ApiPayloadCommon evt = dbStorage.getAnyQueuedEvent();
                    while (evt != null) {
                        Logger.info("Event Processor", "Event id is " + evt.apiEndpointId + "Name:" + evt.apiEndpointName);

                        HttpUtilV2 httpUtil = new HttpUtilV2(context);
                        httpUtil.sendOverHttp(evt);

                        // Remove event from db
                        dbStorage.removeEventFromDb(evt.apiEndpointId);

                        evt = dbStorage.getAnyQueuedEvent();
                    }

                    Logger.info("Queue flushed.");

                } catch (Exception t) {
                    Logger.debug("EventProcessor", "An exception occurred while clearing queue, will retry: ", t);
                    _scheduleQueueFlush(15);
                }
            });
        }
    }

    void removeExpiredEventsFromDb() {
        this.dbStorage.removeExpiredEventsFromDb();
    }

    private void _postAsyncSafely(final String name, final Runnable runnable) {
        try {
            es.execute(runnable);
        } catch (Throwable t) {
            Logger.info("Failed to submit task to the executor service", t);
        }
    }
}
