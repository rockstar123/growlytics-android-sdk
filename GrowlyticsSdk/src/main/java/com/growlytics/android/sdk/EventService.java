package com.growlytics.android.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


class EventService {

    private final Context context;
    private Config config;
    private DeviceInfo deviceInfo;
    private EventQueueService eventQueue = null;

    private ExecutorService es;

    // Sqlite Helper & Syncing
    private DbStorageService dbStorage;

    // App launch event variables
    private TrafficSourceInfo trafficSourceInfo = null;

    private static EventService instance = null;

    /************************** Singleton Class Methods *******************************/

    private EventService(final Context context) {
        this.context = context.getApplicationContext();
        this.config = Config.getInstance(context);

        this.deviceInfo = new DeviceInfo(context);
        this.eventQueue = EventQueueService.getInstance(context);
        this.dbStorage = DbStorageService.getInstance(context);
        this.es = Executors.newFixedThreadPool(1);
    }

    static EventService getInstance(Context context) {
        if (EventService.instance != null) {
            return EventService.instance;
        }

        // Initialize instance.
        EventService.instance = new EventService(context);
        return instance;
    }

    /************************** Activity Callback Methods *******************************/

    void onActivityCreated(final Activity activity) {
        _postAsyncSafely("OnActivityCreated", () -> {
            // Read deep link if app launch
            try {

                Logger.debug("EventProcessor", "Activity Created.");
                Intent intent = activity.getIntent();
                Uri deepLink = intent.getData();

                // Track notification clicked event
                GrwNotification notif = new GrwNotification(intent.getExtras());
                if (intent.getExtras() != null && notif.isFromGrowlytics()) {
                    if (!intent.getExtras().getBoolean(Constants.NOTIFICATION_CLICK_CAPTURED)) {
                        // Track notification clicked event
//                        trackNotificationClicked(notif.getTrackingId(), null);
                    }

                    // Read new deep-link provided in push notification
                    try {
                        deepLink = Uri.parse(notif.getDeepLink());
                    } catch (Throwable t) {
                        // Ignore
                    }
                }

                // Track deeplink if provided.
                if (deepLink != null) {
                    Logger.debug("EventProcessor", "Processing deeplink.");
                    _processDeepLink(deepLink);
                }

            } catch (Throwable t) {
                // Ignore
                Logger.error("EventProcessor", "Failed to process activity created event", t);
            }
        });
    }

    void onActivityResumed(final Activity activity) {

        _postAsyncSafely("OnActivityResumed", () -> {
            try {
                Logger.info("OnActivityResume", "App in foreground");

                // Read Activity Name
                PackageManager packageManager = activity.getPackageManager();
                ActivityInfo activityInfo = packageManager.getActivityInfo(activity.getComponentName(), 0);
                String activityName = activityInfo.name;

                // Handle app launched logic
                AppLaunchedHandler appLaunchedHandler = AppLaunchedHandler.getInstance(context.getApplicationContext());
                appLaunchedHandler.handleAppLaunched(activityName, null);
                FCMTokenService handler = FCMTokenService.getInstance(context);
                handler.handleFCMToken();
                // check for any in-app notifications to show.
//                    if(!(currentActivity instanceof InAppMessageActivity)) {
//                        // Will not spawn thread, current thread will be used.
//                        InAppMessageHandler.getInstance(context).showPendingInAppNotifications();
//                    }
            } catch (Throwable e) {
                Logger.info("Error", "Failed process activity resume event.", e);
            }
        });
    }

    void onActivityPaused() {

        Logger.info("Activity Pause", "App going in background");
//        this.isAppInForeground = false;

        _postAsyncSafely("OnActivityPaused", () -> {
            try {

                // Set last background time
                Logger.info("Activity Pause", "Update session info's last background time.");
                SessionService sessionService = new SessionService(context);
                sessionService.updateSessionLastBackgroundTime();

            } catch (Throwable e) {
                Logger.info("Error", "Failed to update session for activity paused.", e);
            }
        });

    }

    void login(final String uniqueCustomerId, final Map<String, Object> profileAttributes) {
        String logPrefix = "loginEvent";

        _postAsyncSafely("queueEvent", () -> {
            try {

                // Check if plugin is disabled.
                if (Config.getInstance(context).isDisabled()) {
                    Logger.info("Growlytics is disabled from config.");
                    return;
                }

                SessionService service = new SessionService(context);
                DeviceInfo deviceInfo = new DeviceInfo(context);
                SessionInfo sessionInfo = service.getSessionInfo();
                String deviceId = DeviceService.getDeviceId(context);

                // Build payload for server.
                ApiPayloads.LoginUserPayload payload = new ApiPayloads.LoginUserPayload();
                payload.deviceId = deviceId;
                payload.clientCustomerId = uniqueCustomerId;
                payload.deviceInfo = deviceInfo;
                payload.sessionInfo = sessionInfo;

                // Build User Attributes Array
                if (profileAttributes != null) {

                    // Validate Attributes
                    String result = ValidationUtil.validateEventAttributes(profileAttributes);
                    if (result != null) {
                        Log.e("Growlytics", "Analytics.pushProfile() called with invalid customer attribute. Event will be ignored. Reason: " + result);
                        return;
                    }
                    Logger.info(logPrefix, "Session does not exist or expired, will trigger app launch event.");

                    // Build json of attributes
                    JSONArray attributes = new JSONArray();
                    for (String key : profileAttributes.keySet()) {
                        Object value = profileAttributes.get(key);
                        JSONObject attributeInfo = new JSONObject();
                        try {
                            attributeInfo.put("name", key);
                            attributeInfo.put("value", value);
                            attributeInfo.put("type", value.getClass().getSimpleName().toLowerCase());
                            attributes.put(attributeInfo);
                        } catch (JSONException e) {
                            Logger.error("Failed to put attribute into json object, will be skipped. Key: " + key, e);
                        }
                    }

                    payload.attributes = attributes;
                }

                // Save sid to pass further in apis.
                DbStorageService.saveSetting(this.context, DbSettingKey.LoggedInUserId, uniqueCustomerId);

                // Add event to queue.
                eventQueue.addEventToQueue(payload);

            } catch (Throwable t) {
                Logger.error("Failed to login user.", t);
            }
        });
    }

    void logOut() {
        String logPrefix = "logoutEvent";
        _postAsyncSafely("queueEvent", () -> {
            try {

                // Check if plugin is disabled.
                if (Config.getInstance(context).isDisabled()) {
                    Logger.info("Growlytics is disabled from config.");
                    return;
                }

                // If user has already logged out or has not logged in return.
                String loggedInUserId = DbStorageService.getSetting(context, DbSettingKey.LoggedInUserId);
                if(loggedInUserId == null) {
                    Logger.error(logPrefix, "Logout ignored. User has already logged out or has not logged in yet.");
                    return;
                }

                SessionService service = new SessionService(context);
                DeviceInfo deviceInfo = new DeviceInfo(context);
                SessionInfo sessionInfo = service.getSessionInfo();
                String deviceId = DeviceService.getDeviceId(context);

                // Build Logout Api Payload
                ApiPayloads.LogoutUserPayload payload = new ApiPayloads.LogoutUserPayload();
                payload.deviceId = deviceId;
                payload.deviceInfo = deviceInfo;
                payload.sessionInfo = sessionInfo;
                payload.clientCustomerId = loggedInUserId;

                // Add Logout Api Event to queue.
                eventQueue.addEventToQueue(payload);

                // Remove logged in user id from db.
                DbStorageService.removeSetting(this.context, DbSettingKey.LoggedInUserId);

            } catch (Throwable t) {
                Logger.error("Failed to logout user.", t);
            }
        });
    }

    void trackCustomEvent(final String eventName, final Map<String, Object> eventAttributes) {
//        _postAsyncSafely("queueEvent", new Runnable() {
//            @Override
//            public void run() {
//                try {
//
//                    // Validate event name
//                    String result = ValidationUtil.validateEventName(eventName);
//                    if (result != null) {
//                        Log.e("Growlytics", "Invalid Event Name. Event reporting aborted. Reason: " + result);
//                        return;
//                    }
//
//                    // Build event object
//                    Event event = new Event();
//                    event.name = eventName;
//                    event.eventType = Constants.EventType.CustomEvent;
//                    event.eventTime = System.currentTimeMillis() / 1000;
//
//                    // Process attributes if provided.
//                    if (eventAttributes != null) {
//
//                        // Validate Attributes
//                        result = ValidationUtil.validateEventAttributes(eventAttributes);
//                        if (result != null) {
//                            Log.e("Growlytics", "Analytics.pushEvent() called with invalid attributes. Event will be ignored. Reason: " + result);
//                            return;
//                        }
//
//                        // Build json of attributes
//                        JSONObject info = new JSONObject();
//                        for (String key : eventAttributes.keySet()) {
//                            Object value = eventAttributes.get(key);
//                            JSONObject attributeInfo = new JSONObject();
//                            try {
//                                attributeInfo.put("name", key);
//                                attributeInfo.put("value", value);
//                                attributeInfo.put("type", value.getClass().getSimpleName().toLowerCase());
//                                info.put(key, attributeInfo);
//                            } catch (JSONException e) {
//                                Logger.error("Failed to put attribute into json object. Key: " + key, e);
//                            }
//                        }
//                        event.info = info;
//                    }
//
//                    // Add event to queue.
//                    eventQueue.addEventToQueue(event);
//
//                } catch (Throwable t) {
//                    // We won't get here
//                    Logger.error("EventProcessor", "Failed to process custom event track request.", t);
//                }
//
//            }
//        });
    }

    private void _processDeepLink(Uri deepLink) {
        try {
            JSONObject utmInfo = UriUtil.parseUtmInfo(deepLink);
            String referrer = deepLink.toString();

            // Put processed json to local variable to be sent with app launch
            this.trafficSourceInfo = new TrafficSourceInfo(referrer, utmInfo);

        } catch (Throwable t) {
            Logger.error("Event Processor", "Failed to push deep link", t);
        }
    }

    /************************** Process Notification Events *******************************/

//    void trackNotificationViewed(final String trackingId) {
//        if (trackingId == null || trackingId.isEmpty()) {
//            Logger.info("Notification", "Notification Tracking Id is missing. Notification viewed event will not be tracked.");
//            return;
//        }
//
//        _postAsyncSafely("queueEvent", () -> {
//            try {
//                Event evt = new Event();
//                evt.name = "Notification Viewed";
//                evt.eventType = Constants.EventType.NotificationImpression;
//                evt.waitForSession = false;
//                evt.eventTime = System.currentTimeMillis() / 1000;
//                evt.info = new JSONObject();
//                evt.info.put("id", trackingId);
////                    eventQueue.addEventToQueue(evt);
//                Logger.info("Notification", "Added notification viewed  event to event queue.");
//            } catch (Throwable t) {
//                Logger.error("Notification", "Failed to send notification viewed report.", t);
//            }
//        });
//    }

//    void trackNotificationClicked(final String trackingId, final String actionId) {
//
//        if (trackingId == null || trackingId.isEmpty()) {
//            Logger.info("Notification", "Notification Tracking Id is missing. Notification clicked event will not be tracked.");
//            return;
//        }
//
//        _postAsyncSafely("queueEvent", () -> {
//            try {
//                Event evt = new Event();
//                evt.name = "Notification Viewed";
//                evt.eventType = Constants.EventType.NotificationClicked;
//                evt.eventTime = System.currentTimeMillis() / 1000;
//                evt.waitForSession = false;
//                evt.info = new JSONObject();
//                evt.info.put("id", trackingId);
//                if (actionId != null) {
//                    evt.info.put("actionId", actionId);
//                }
////                eventQueue.addEventToQueue(evt);
//                Logger.info("Notification", "Added notification click event to event queue.");
//            } catch (Throwable t) {
//                Logger.error("Notification", "Failed to send notification viewed report.", t);
//            }
//        });
//    }

    //----- Other Private Methods

    private void _postAsyncSafely(final String name, final Runnable runnable) {
        try {
            // Original Logic
//            final boolean executeSync = Thread.currentThread().getId() == EXECUTOR_THREAD_ID;
//
//            if (executeSync) {
//                runnable.run();
//            } else {
//                es.submit(new Runnable() {
//                    @Override
//                    public void run() {
//                        EXECUTOR_THREAD_ID = Thread.currentThread().getId();
//                        try {
//                            runnable.run();
//                        } catch (Throwable t) {
//                            Logger.info("Executor service: Failed to complete the scheduled task", t);
//                        }
//                    }
//                });
//            }
            es.execute(runnable);
        } catch (Throwable t) {
            Logger.info("Failed to submit task to the executor service", t);
        }
    }

}