package com.growlytics.android.sdk;

import android.content.Context;

import androidx.annotation.RestrictTo;

import com.google.firebase.messaging.FirebaseMessaging;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RestrictTo({RestrictTo.Scope.LIBRARY, RestrictTo.Scope.LIBRARY_GROUP})
class FCMTokenService {

    private static FCMTokenService instance = null;
    private final Context context;
    private final ExecutorService es;
    private final EventQueueService eventQueue;
    DeviceInfo deviceInfo;
    private final Object fcmTokenPushedLock = new Object();
    private static boolean fcmTokenPushed = false;

    public FCMTokenService(Context context) {
        this.context = context.getApplicationContext();
        this.es = Executors.newFixedThreadPool(1);
        this.eventQueue = EventQueueService.getInstance(context);
        deviceInfo = new DeviceInfo(context);
    }

    static FCMTokenService getInstance(Context context) {
        if (instance != null) {
            return instance;
        }
        Logger.info("FCMTokenHandler Instance is initializing.");
        instance = new FCMTokenService(context);
        return instance;
    }

    void handleFCMToken() {
        String logPrefix = "AppLaunch:";
        synchronized (fcmTokenPushedLock) {
            String deviceId = DeviceService.getDeviceId(this.context);
            if (deviceId == null) {
                Logger.info(logPrefix, "Device id not found, generate & sync device id.");
                deviceId = DeviceService.generateAndSyncDeviceId(this.context, this.deviceInfo);
                // Track app install for new device id.
                Logger.info(logPrefix, "Check app install for new device id.");
            }

            // clear expired events from db
            eventQueue.removeExpiredEventsFromDb();

            SessionService sessionService = new SessionService(this.context);
            if (!sessionService.isSessionExpired() && FCMTokenService.fcmTokenPushed) {
                Logger.info(logPrefix, "SessionInfo Exists & App Launched has already been triggered. Will not trigger it.");
                return;
            }
            String finalDeviceId = deviceId;
            FirebaseMessaging.getInstance().getToken().addOnSuccessListener(token -> {
                Logger.info(logPrefix, "FCM Token: " + token);
                if (token != null) {
                    this.updateFcmToken(finalDeviceId, sessionService.getSessionInfo(), token);
                }
            });
            FCMTokenService.fcmTokenPushed = true;
        }
    }

    void updateFcmToken(String deviceId, SessionInfo sessionInfo, String token) {
        DeviceInfo deviceInfo = this.deviceInfo;
        _postAsyncSafely("UpdateToken", () -> {

            Logger.debug("FCM Token Received:", token);

            // Check if already reported in last 1 hour, if so, return.
            String lastReportedTimestampStr = DbStorageService.getSetting(context, DbSettingKey.FcmTokenReportedTimestamp);
            if (lastReportedTimestampStr != null) {
                long lastReportedTimestamp = Long.parseLong(lastReportedTimestampStr);
                if (lastReportedTimestamp < (System.currentTimeMillis() - 60 * 60 * 1000)) {
                    return;
                }
            }

            try {
                // Report Token To Server.
                ApiPayloads.SaveFcmTokenPayload payload = new ApiPayloads.SaveFcmTokenPayload();
                payload.deviceId = deviceId;
                payload.deviceInfo = deviceInfo;
                payload.sessionInfo = sessionInfo;
                payload.token = token;
                // TODO: Add SID If Available.
                Logger.debug("AppLaunch:", "Adding Firebase Token event to queue." + payload);
                EventQueueService.getInstance(context).addEventToQueue(payload);

                // Save Last Reported Time In Db.
                DbStorageService.saveSetting(context, DbSettingKey.FcmTokenReportedTimestamp, String.valueOf(System.currentTimeMillis()));
            } catch (Throwable t) {
                Logger.error("AppLaunch:", "Failed to report firebase token to server.", t);
            }
        });
    }

    private void _postAsyncSafely(final String name, final Runnable runnable) {
        try {
            // Original Logic
//            final boolean executeSync = Thread.currentThread().getId() == EXECUTOR_THREAD_ID;
//
//            if (executeSync) {
//                runnable.run();
//            } else {
//                es.submit(new Runnable() {
//                    @Override
//                    public void run() {
//                        EXECUTOR_THREAD_ID = Thread.currentThread().getId();
//                        try {
//                            runnable.run();
//                        } catch (Throwable t) {
//                            Logger.info("Executor service: Failed to complete the scheduled task", t);
//                        }
//                    }
//                });
//            }
            es.execute(runnable);
        } catch (Throwable t) {
            Logger.info("Failed to submit task to the executor service", t);
        }
    }
}
