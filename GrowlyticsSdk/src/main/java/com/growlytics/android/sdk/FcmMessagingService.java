package com.growlytics.android.sdk;

import android.content.Context;

import androidx.annotation.NonNull;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class FcmMessagingService extends FirebaseMessagingService {

    public static void updateFCMToken(String token, Context context) {

        if (Config.getInstance(context).isDisabled()) {
            Logger.info("Growlytics is disabled from config.");
            return;
        }

        String deviceId = DeviceService.getDeviceId(context);

        SessionService ss = new SessionService(context);
        SessionInfo sessionInfo = ss.getSessionInfo();

        Logger.info("FCM", "New Token event received. => " + token);

        FCMTokenService.getInstance(context.getApplicationContext()).updateFcmToken(deviceId, sessionInfo, token);
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        updateFCMToken(s, getApplicationContext());
    }

    public static void onMessageReceived(RemoteMessage message, Context context) {

        if (Config.getInstance(context).isDisabled()) {
            Logger.info("Growlytics is disabled from config.");
            return;
        }

        Logger.debug("GrwNotification", "New GrwNotification received." + message.getData().toString());

        // If Growlytics tag not found return.
        GrwNotification notif = new GrwNotification(message);

        // If no data available, abort.
        if (notif.getData().size() == 0) {
            return;
        }

        if (notif.isFromGrowlytics() && notif.isInAppNotification()) {
            // Add notification to queue.
            Logger.debug("GrwNotification", "In App Notification Found");
//            InAppMessageHandler.getInstance(context).queueInAppNotification(notif.getInAppNotificationContent());
        } else if (notif.isFromGrowlytics() && notif.isDoRender()) {
            NotificationUtil.getInstance().renderNotification(context, notif);
//            EventService.getInstance(context).trackNotificationViewed(notif.getTrackingId());
        } else {
            Logger.debug("GrwNotification", "Notification Ignored. Not-Growlytics notification or do not render flag is present.");
        }

    }

    @Override
    public void onMessageReceived(RemoteMessage message) {
        super.onMessageReceived(message);

        onMessageReceived(message, getApplicationContext());

    }
}
