package com.growlytics.android.sdk;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Growlytics {

    // Singleton instance
    private static Growlytics instance = null;
    private Config config = null;

    private final EventService eventService;
    // Async background processing variables
    private long EXECUTOR_THREAD_ID = 0;
    private ExecutorService es;
    private Context context;
    private Handler handlerUsingMainLooper;

    /********************* Singleton Instance ************************************/

    private Growlytics(final Context context) {
        this.context = context;
        this.config = Config.getInstance(context);
        this.es = Executors.newFixedThreadPool(1);
        this.handlerUsingMainLooper = new Handler(Looper.getMainLooper());

        this.eventService = EventService.getInstance(context);

    }

    public static Growlytics getInstance(Context context) {

        if (instance != null) {
            return instance;
        }

        // Initialize instance.
        instance = new Growlytics(context);
        return instance;
    }

    /********************* End: Singleton Instance ************************************/

    /********************* Start: API Methods ************************************/

    public void track(final String eventName) {
        if (Config.getInstance(context).isDisabled()) {
            Logger.info("Growlytics is disabled from config.");
            return;
        }
        this.track(eventName, null);
    }


    /**
     * Track an event with attributes(in key value pair).
     * <p>
     * DataType validation for event Attributes:
     * Attribute Key: Key must be string not more than 100 characters.
     * Attribute Values: The value of attribute can be of type String, Integer, Long, Double, Float, Boolean, Character and Date(java.util.date) only.
     * On Attribute validation failure, error message will be printed as warning and event will be ignored and will not be sent to Growlytics server.
     *
     * @param eventName       The name of the event
     * @param eventAttributes A {@link HashMap}, with keys as strings, and values as {@link String},
     *                        {@link Integer}, {@link Long}, {@link Boolean}, {@link Float}, {@link Double},
     *                        {@link java.util.Date}, or {@link Character}
     */
    public void track(final String eventName, Map<String, Object> eventAttributes) {

        if (Config.getInstance(context).isDisabled()) {
            Logger.info("Growlytics is disabled from config.");
            return;
        }

        this.eventService.trackCustomEvent(eventName, eventAttributes);

    }

    /**
     * Login user with unique customer id and profile Attributes.
     * <p>
     *
     * @params uniqueCustomerId, profileAttributes
     */
    public void login(final String uniqueCustomerId, final Map<String, Object> profileAttributes) {

        if (Config.getInstance(context).isDisabled()) {
            Logger.info("Growlytics is disabled from config.");
            return;
        }

        this.eventService.login(uniqueCustomerId, profileAttributes);

    }

    /**
     * Logout existing logged in user.
     * <p></p>
     *
     */
    private void logOut() {

        if (Config.getInstance(context).isDisabled()) {
            Logger.info("Growlytics is disabled from config.");
            return;
        }
        this.eventService.logOut();
    }

    /********************* End: API Methods ************************************/

    /********************* Start: Activity Methods ************************************/

    void onActivityCreated(Activity activity) {

        if (Config.getInstance(context).isDisabled()) {
            Logger.info("Growlytics is disabled from config.");
            return;
        }

        if (instance == null) {
            Logger.info("Instance is null in onActivityResumed!");
            return;
        }

        this.eventService.onActivityCreated(activity);
    }

    void onActivityResumed(Activity activity) {

        if (Config.getInstance(context).isDisabled()) {
            Logger.info("Growlytics is disabled from config.");
            return;
        }

        if (instance == null) {
            Logger.info("Instance is null in onActivityResumed!");
            return;
        }

        this.eventService.onActivityResumed(activity);
    }

    void onActivityPaused() {

        if (Config.getInstance(context).isDisabled()) {
            Logger.info("Growlytics is disabled from config.");
            return;
        }

        if (instance == null) {
            Logger.info("Instance is null in onActivityPaused!");
            return;
        }

        this.eventService.onActivityPaused();
    }

    /********************* Start: Activity Methods ************************************/

    /**************************** Private Common Methods *******************************************/

}