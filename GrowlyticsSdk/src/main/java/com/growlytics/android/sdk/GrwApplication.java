package com.growlytics.android.sdk;


import android.app.Application;

public class GrwApplication extends Application {

    @Override
    public void onCreate() {

        if (Config.getInstance(getApplicationContext()).isDisabled()) {
            Logger.info("Growlytics is disabled from config.");
            return;
        }
        // Register callbacks
        ActivityLifecycleCallback.register(this);

        super.onCreate();
    }
}