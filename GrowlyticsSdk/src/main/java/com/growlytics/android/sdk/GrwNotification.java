package com.growlytics.android.sdk;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Map;

import static android.content.Context.NOTIFICATION_SERVICE;

@SuppressWarnings("WeakerAccess")
public final class GrwNotification {

    private boolean isFromGrowlytics;
    private boolean doRender;
    private Bundle data = null;

    private String trackingId = null;
    private String notifiaticationChannel = "";
    private String priority;
    private JSONArray actionButtons;
    private String ttl;
    private String title = null;
    private String message = null;
    private String imageUrl = null;
    private String subTitle = null;

    private boolean isInAppNotification = false;
    private JSONObject inAppNotificationContent = null;

    private String smallIconColor = null;

    private String deepLink = null;

    public GrwNotification(RemoteMessage message) {

        try {
//            JSONArray dd = new JSONArray();
//            try {
//                for (int i = 0; i < 3; i++) {
//                    JSONObject di = new JSONObject();
//
//                    di.put("l", "Button" + (i + 1));
//                    di.put("dl", "http://www.growlytics.com/ravi");
//                    di.put("ico", "");
//                    di.put("id", String.valueOf(i));
//                    di.put("ac", "");
//                    dd.put(di);
//
//                }
//            } catch (Throwable t) {
//
//            }
//
//            // Prepare dummy data
//            Bundle data = new Bundle();
//            data.putString("glytcs_pnf", "");
////        data.putString("glytcs_dnr", "");
//            data.putString("glytcs_tl", "My title");
//            data.putString("glytcs_msg", "My Message");
//            data.putString("glytcs_img", "https://images.unsplash.com/photo-1558981806-ec527fa84c39");
//            data.putString("glytcs_sbt", "Subtitle here");
//            data.putString("glytcs_pr", "high");
//            data.putString("channel", "ravi");
//            data.putString("glytcs_clr", "#5c32a8");
//            data.putString("glytcs_dl", "");
//            data.putString(Constants.NOTIFICATION_ACTION_BTNS, dd.toString());
//            this.data = data;
            Bundle data = new Bundle();
            for (Map.Entry<String, String> entry : message.getData().entrySet()) {
                data.putString(entry.getKey(), entry.getValue());
            }
            this.data = data;
            _init();
        } catch (Throwable t) {
            // Ignore
        }
    }

    GrwNotification(Bundle data) {
        this.data = data;
        _init();
    }

    private void _init() {
        try {
            this.isFromGrowlytics = this.data.containsKey("glytcs_pnf");
            this.doRender = !this.data.containsKey("glytcs_dnr");
            this.trackingId = this.data.getString("glytcs_id");

            this.isInAppNotification = this.data.containsKey("glytcs_in_app");
            if (isInAppNotification) {
                JSONObject inAppNotif = new JSONObject();
                inAppNotif.put("id", this.trackingId);
                inAppNotif.put("content", new JSONObject(this.data.getString("glytcs_in_app_content")));
                this.inAppNotificationContent = inAppNotif;

                // If in app notif, dont process rest of the fields.
                return;
            }

            this.title = this.data.getString("glytcs_tl");
            this.message = this.data.getString("glytcs_msg");
            this.imageUrl = this.data.getString("glytcs_img");
            this.subTitle = this.data.getString("glytcs_sbt");
            this.priority = this.data.getString("glytcs_pr");

            this.notifiaticationChannel = this.data.getString("glytcs_chnl");
            this.smallIconColor = this.data.getString("glytcs_clr");
            this.deepLink = this.data.getString("glytcs_dl");

            String defaultTTL = (System.currentTimeMillis() + Constants.NOTIFICATION_DEFAULT_TTL) / 1000 + "";
            this.ttl = this.getData().getString(Constants.NOTIFICATION_TTL, defaultTTL);

            String buttons = this.data.getString(Constants.NOTIFICATION_ACTION_BTNS);
            if (buttons != null && !buttons.trim().isEmpty()) {
                try {
                    this.actionButtons = new JSONArray(buttons);
                } catch (Throwable t) {
                    // Ignore
                    Logger.error("Notification", "Failed to parse action buttons.", t);
                }
            }
        } catch (Throwable t) {
            // Ignore
        }
    }

    boolean isValidChannel(Context context, String channelName) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
            if (notificationManager == null)
                return false;
            if (channelName.isEmpty()) {
                return false;
            } else if (notificationManager.getNotificationChannel(channelName) == null) {
                return false;
            }
            return true;
        } else {
            return true;
        }
    }

    JSONObject getInAppNotificationContent() {
        return inAppNotificationContent;
    }

    boolean isInAppNotification() {
        return isInAppNotification;
    }

    boolean isFromGrowlytics() {
        return isFromGrowlytics;
    }

    String getTrackingId() {
        return trackingId;
    }

    public String getDeepLink() {
        return deepLink;
    }

    String getTtl() {
        return ttl;
    }

    boolean isDoRender() {
        return doRender;
    }

    JSONArray getActionButtons() {
        return actionButtons;
    }

    String getSmallIconColor() {
        return smallIconColor;
    }

    String getPriority() {
        return priority;
    }

    Bundle getData() {
        return data;
    }

    String getNotifiaticationChannel() {
        return notifiaticationChannel;
    }

    String getTitle() {
        return title;
    }

    String getSubTitle() {
        return subTitle;
    }

    String getMessage() {
        return message;
    }

    String getImageUrl() {
        return imageUrl;
    }
}
