package com.growlytics.android.sdk;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

/**
 * Android 12 prevents components that start other activities inside services or broadcast receivers when notification is opened.
 * If this requirement is not met, the system will prevent the activity from starting.
 * <p>
 * As a part of these Android OS changes, CleverTap will be deprecating the CTPushNotificationReceiver class from v4.3.0
 * <p>
 * CTPushNotificationReceiver was used to handle Push notifications with deep links pointing inside the app or third-party applications/URLs.
 * The receiver would first raise the Notification Clicked event and then open the Activity as mentioned in the deep link.
 * <p>
 * Android 12 restricts usage of notification trampolines, meaning that notification must start the activity directly on the notification tap.
 * Tracking of Notification Clicked events now happens in the {@link Growlytics} }instance registered by {@link com.growlytics.android.sdk.ActivityLifecycleCallback}.
 * The push payload is attached as an extra to the notification intent and processed when the notification is tapped.
 * If the deep link is to a third-party application or a URL, then the Notification Clicked event will NO LONGER be tracked.
 */
public class GrwNotificationIntentService extends IntentService {

    private final static String TAG = "NotifictionIntentService";

    public final static String MAIN_ACTION = "com.growlytics.TRACK_NOTIFICATION";
    public final static String TYPE_BUTTON_CLICK = "com.growlytics.ACTION_BUTTON_CLICK";

    public GrwNotificationIntentService() {
        super("GrwNotificationIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if (Config.getInstance(getApplicationContext()).isDisabled()) {
            Logger.info("Growlytics is disabled from config.");
            return;
        }

        Bundle extras = intent.getExtras();
        if (extras == null) return;

        // Check if its growlytics notification click
        GrwNotification grwNotification = new GrwNotification(extras);
        if (grwNotification.isFromGrowlytics()) {
            Logger.info(TAG, "Start: Track notification Button clicked.");
            handleNotificationButtonClick(grwNotification);
            return;
        }

        Logger.info(TAG, "Ignored intent, it's not a Growlytics triggered notification.");
    }

    private void handleNotificationButtonClick(GrwNotification notif) {
        try {

//            // Read dta specified for specific buttons
//            int notificationId = notif.getData().getInt("notificationId", -1);
//            String dl = notif.getData().getString("dl");
//            boolean autoCancel = notif.getData().getBoolean("ac", false);
//
//            Context context = getApplicationContext();
//
//            // Build intent
//            Intent launchIntent;
//            if (dl != null) {
//                launchIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(dl));
//            } else {
//                launchIntent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
//            }
//
//            if (launchIntent == null) {
//                Logger.info(TAG, "create launch intent.");
//                return;
//            }
//
//            launchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            launchIntent.putExtras(notif.getData());
//            launchIntent.removeExtra("dl");
//
//            // Close notification if specified to do.
//            if (autoCancel && notificationId > -1) {
//                NotificationManager notificationManager =
//                        (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
//                if (notificationManager != null) {
//                    notificationManager.cancel(notificationId);
//                }
//            }
//            sendBroadcast(new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)); // close the notification drawer
//
//            // Start Activity
//            launchIntent.putExtra(Constants.NOTIFICATION_CLICK_CAPTURED, true);
//            startActivity(launchIntent);
//
//            // Report Notification Click
//            EventService.getInstance(getApplicationContext()).trackNotificationClicked(notif.getTrackingId(), "1");

        } catch (Throwable t) {
            Logger.error(TAG, "Failed to handle button click.", t);
        }
    }
}
