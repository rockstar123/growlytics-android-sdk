package com.growlytics.android.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;


public class GrwPushNotificationReceiver extends BroadcastReceiver {

    private static String TAG = "GrwPushNotificationReceiver";

    public static void onNotificationReceived(Context context, Intent intent){
        if (Config.getInstance(context).isDisabled()) {
            Logger.info("Growlytics is disabled from config.");
            return;
        }

        try {
            Logger.debug(TAG, "Receiver called for notification." + intent.getExtras().toString());

            Bundle extras = intent.getExtras();
            if (extras == null) {
                return;
            }
            GrwNotification notif = new GrwNotification(intent.getExtras());

            Intent launchIntent;
            if (notif.isFromGrowlytics()) {
                if (notif.getDeepLink() != null) {
                    launchIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(notif.getDeepLink()));
                } else {
                    launchIntent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
                    if (launchIntent == null) {
                        return;
                    }
                }

                launchIntent.putExtra(Constants.NOTIFICATION_CLICK_CAPTURED, true);

            } else {
                launchIntent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
            }

            if (launchIntent == null) {
                launchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                launchIntent.putExtras(extras);
            }

            context.startActivity(launchIntent);
            Logger.debug(TAG, "Activity Started.");

            // Track event, only if its rendered and is from growlytics.
            if (notif.isFromGrowlytics() && notif.isDoRender()) {
//                EventService.getInstance(context).trackNotificationClicked(notif.getTrackingId(), null);
            }
        } catch (Throwable t) {
            // Ignore
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        onNotificationReceived(context, intent);
    }
}
