package com.growlytics.android.sdk;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.RestrictTo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.net.ssl.HttpsURLConnection;

@RestrictTo(RestrictTo.Scope.LIBRARY)
public class HttpUtilV2 {

    private ExecutorService es;
    private Handler handlerUsingMainLooper;

    DeviceInfo deviceInfo;
    private Context context;
    private Config config;

    // Sqlite Helper & Syncing
    private DbStorageService dbStorage;

    public HttpUtilV2(final Context context) {
        this.context = context;
        this.config = Config.getInstance(context);
        this.es = Executors.newFixedThreadPool(5);
        this.dbStorage = DbStorageService.getInstance(context);
        this.handlerUsingMainLooper = new Handler(Looper.getMainLooper());
        this.deviceInfo = new DeviceInfo(context);
    }

    public void sendOverHttp(ApiPayloads.ApiPayloadCommon apiInput) throws IOException, JSONException {

        HttpsURLConnection conn = null;
        try {

            Logger.info("Network", "Start: Send event to network:" + apiInput.apiEndpointName);

            // Read Url.
            UrlList urlList = new UrlList();
            String location = config.getHost() + urlList.getUrl(apiInput.apiEndpointName);
//            Logger.debug("Network", "Url: " + location);

            // Prepare Connection
            URL url = new URL(location);
            conn = (HttpsURLConnection) url.openConnection();
            conn.setConnectTimeout(120 * 1000); // Seconds * 1000
            conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestProperty("x-growlytics-key", this.config.getApiKey());
            conn.setInstanceFollowRedirects(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Accept", "application/json");

            // Todo: SSL Pinning is pending
//            Logger.debug("Network", "Pinning SSL");
//            SSLContext sslContext = new SSLContextBuilder().build();
//            conn.setSSLSocketFactory(sslContext.getSocketFactory());
//            Logger.debug("Network", "SSL Pinned");

            // Build request body
            Logger.debug("Network", "Start: Request: " + url + ": " + apiInput.apiPayload);

            // Write data to connection stream.
            JSONObject bodyJson = new JSONObject(apiInput.apiPayload);
            bodyJson.put("timestamp", System.currentTimeMillis());
            String httpPayload = bodyJson.toString();
            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            Logger.debug("Network", "Writer stream created. Sending Request.");
            wr.writeBytes(httpPayload);

            // Always check for a 200 OK
            final int responseCode = conn.getResponseCode();
            if (responseCode != 200) {

                // If invalid api key combination, disable plugin.
                if (responseCode == 401) {
                    // Disable plugin, and ignore all the current events.
                    config.setDisabled(true);
                    Logger.info("Network", "Un-authorized response. Event ignored. Disabling plugin.");
                }

                throw new IOException("Response code is not 200. It is " + responseCode);
            }
            Logger.info("Network", "Network success response");

        } catch (java.net.SocketTimeoutException e) {
            Logger.error("Network", "An exception occurred while sending the queue, will retry.", e);            // Network error
            throw e;
        } catch (IOException e) {
            Logger.error("Network", "An exception occurred while sending the queue, will not retry.", e);

        } catch (Exception e) {
            Logger.error("Network", "An exception occurred while sending the queue, will not retry.", e);
        } finally {
            if (conn != null) {
                try {
                    conn.getInputStream().close();
                } catch (Throwable t) {
                    // Ignore
                }
            }
        }
    }


    private class UrlList {

        public String getUrl(String eventName) throws Exception {
            if (ApiPayloads.ApiEndpointNames.RegisterDevice.equals(eventName)) {
                return "/events/mobile-sdk-device";
            } else if (ApiPayloads.ApiEndpointNames.CreateSession.equals(eventName)) {
                return "/events/mobile-sdk-session";
            } else if (ApiPayloads.ApiEndpointNames.CreateEvent.equals(eventName)) {
                return "/events/mobile-sdk-event";
            } else if (ApiPayloads.ApiEndpointNames.Login.equals(eventName)) {
                return "/events/mobile-sdk-login";
            } else if (ApiPayloads.ApiEndpointNames.Logout.equals(eventName)) {
                return "/events/mobile-sdk-logout";
            } else if (ApiPayloads.ApiEndpointNames.UpdateCustomer.equals(eventName)) {
                return "/events/mobile-sdk-update";
            } else if (ApiPayloads.ApiEndpointNames.SaveMobilePushToken.equals(eventName)) {
                return "/events/mobile-save-push";
            } else {
                throw new Exception("No url found for event name: " + eventName);
            }
        }
    }
}
