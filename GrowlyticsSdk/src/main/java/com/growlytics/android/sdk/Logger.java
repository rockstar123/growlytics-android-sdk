package com.growlytics.android.sdk;

import android.util.Log;

final class Logger {
    private static boolean enabled = false;
    private static String logIdentifier = "GrowlyticsSdk: ";

    static void setEnabled(boolean trueFalse) {
        enabled = trueFalse;
    }

    /******************* Warn Methods **************************************/

    static void error(final String message) {
        if (!enabled) return;
        Log.e(Logger.logIdentifier, + Thread.currentThread().getId() + ": " + message);
    }

    static void error(final String suffix, final String message) {
        if (!enabled) return;
        Log.e(Logger.logIdentifier,  suffix + " " + Thread.currentThread().getId() + ": " + message);
    }

    static void error(final String suffix, final String message, final Throwable t) {
        if (!enabled) return;
        Log.e(Logger.logIdentifier, suffix + " " + Thread.currentThread().getId() + ":" + message, t);
    }

    static void error(final String message, final Throwable t) {
        if (!enabled) return;
        Log.e(Logger.logIdentifier, Thread.currentThread().getId() + ": " + message, t);
    }

    /******************* Warn Methods **************************************/

    static void warn(final String message) {
        if (!enabled) return;
        Log.w(Logger.logIdentifier, Thread.currentThread().getId() + ": " + message);
    }

    static void warn(final String suffix, final String message) {
        if (!enabled) return;
        Log.w(Logger.logIdentifier, suffix + " " + Thread.currentThread().getId() + ": " + message);
    }

    static void warn(final String suffix, final String message, final Throwable t) {
        if (!enabled) return;
        Log.w(Logger.logIdentifier, suffix + " " + Thread.currentThread().getId() + ": " + message, t);
    }

    static void warn(final String message, final Throwable t) {
        if (!enabled) return;
        Log.w(Logger.logIdentifier, Thread.currentThread().getId() + ": " + message, t);
    }

    /******************* Debug Methods **************************************/

    static void debug(final String message) {
        if (!enabled) return;
        Log.d(Logger.logIdentifier, Thread.currentThread().getId() + ": " + message);

    }

    static void debug(final String suffix, final String message) {
        if (!enabled) return;
        Log.d(Logger.logIdentifier, suffix + " " + Thread.currentThread().getId() + ": " + message);
    }

    static void debug(final String suffix, final String message, final Throwable t) {
        if (!enabled) return;
        Log.d(Logger.logIdentifier,  suffix + " " + Thread.currentThread().getId() + ": " + message, t);
    }

    static void debug(final String message, final Throwable t) {
        if (!enabled) return;
        Log.d(Logger.logIdentifier, Thread.currentThread().getId() + ": " + message, t);
    }

    /******************* Info Methods **************************************/

    static void info(final String message) {
        if (!enabled) return;
        Log.i(Logger.logIdentifier, Thread.currentThread().getId() + ": " + message);
    }

    static void info(final String suffix, final String message) {
        if (!enabled) return;
        Log.i(Logger.logIdentifier,  suffix + " " + Thread.currentThread().getId() + ": " + message);
    }

    static void info(final String suffix, final String message, final Throwable t) {
        if (!enabled) return;
        Log.i(Logger.logIdentifier ,  suffix + " " + Thread.currentThread().getId() + ": " + message, t);
    }

    static void info(final String message, final Throwable t) {
        if (!enabled) return;
        Log.i(Logger.logIdentifier, Thread.currentThread().getId() + ":" +  message, t);
    }
}