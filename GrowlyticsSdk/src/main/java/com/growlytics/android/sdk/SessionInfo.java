package com.growlytics.android.sdk;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

class SessionInfo {

    private String sessionId = null;
    private Integer createdAt = null;
    private Integer lastBackgroundTime = null;
    private Boolean isFirstSession = null;

    SessionInfo(String sessionId, int createdAt, Integer lastBackgroundTime, boolean isFirstSession) {
        this.sessionId = sessionId;
        this.createdAt = createdAt;
        this.lastBackgroundTime = lastBackgroundTime;
        this.isFirstSession = isFirstSession;
    }

    String getSessionId() {
        return sessionId;
    }

    int getCreatedAt() {
        return createdAt;
    }

    boolean isSessionExpired(Context context){
        int now = (int) (System.currentTimeMillis() / 1000);
        int expiryTime = getSessionExpiryTime(context);
        Logger.debug("Session-Expiry", "Session Expiry Time: " + expiryTime + " Now: " + now);
        return expiryTime < now;
    }

    int getSessionExpiryTime(Context context){
        int lastActivityTime = this.lastBackgroundTime != null ?this.lastBackgroundTime : this.createdAt;
        int sessionLength = Config.getInstance(context).getSessionLength();
        return lastActivityTime + (1 * 60 );
    }

    int getSessionLength(Context context){
        return Config.getInstance(context).getSessionLength();
    }

    boolean isFirstSession() {return isFirstSession;}

    void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    void setCreatedAt(int createdAt) {
        this.createdAt = createdAt;
    }

    void setLastBackgroundTime(int lastBackgroundTime) {
        this.lastBackgroundTime = lastBackgroundTime;
    }

    @NonNull
    @Override
    public String toString() {
        JSONObject json = new JSONObject();
        try {
            json.put("sessionId", this.sessionId);
            json.put("createdAt", this.createdAt);
            json.put("lastBackgroundTime", this.lastBackgroundTime);
            json.put("isFirstSession", this.isFirstSession);

            return json.toString();

        } catch (JSONException e) {
            Logger.info("Event-Stringify", "Failed to stringify event.", e);
            // Ignore
            return null;
        }
    }

    static SessionInfo fromJson(String sessionInfoJson) {
        try {

            JSONObject json = new JSONObject(sessionInfoJson);

            String sessionId = json.getString("sessionId");
            int createdAt = json.getInt("createdAt");
            Integer lastBackgroundTime = json.has("lastBackgroundTime") ? json.getInt("lastBackgroundTime") : null;
            boolean isFirstSession = json.getBoolean("isFirstSession");
            SessionInfo evt = new SessionInfo(sessionId, createdAt, lastBackgroundTime, isFirstSession);

            return evt;

        } catch (JSONException e) {
            Logger.info("EventParse", "Failed to parse event.", e);
            return null;
        }
    }
}
