package com.growlytics.android.sdk;

import android.content.Context;


public class SessionService {

    private final Context context;
    private final Object appLaunchPushedLock = new Object();

    public SessionService(final Context context) {
        this.context = context;
    }

    public boolean isSessionExpired() {
        // Check if session is createFd
        SessionInfo sessionInfo = getSessionInfo();
        if (sessionInfo == null) {
            return true;
        }

        return sessionInfo.isSessionExpired(this.context);
    }

    public void updateSessionLastBackgroundTime() {
        SessionInfo sessionInfo = getSessionInfo();
        if (sessionInfo == null) {
            return;
        }

        int now = (int) (System.currentTimeMillis() / 1000);
        sessionInfo.setLastBackgroundTime(now);
        DbStorageService.saveSetting(context, DbSettingKey.SessionInfo, sessionInfo.toString());
    }

    public void createSessionIfExpired(String screenName, DeviceInfo deviceInfo) throws Exception {
        try {
            if (isSessionExpired()) {
                Logger.debug("Session is expired, creating new one.");
                String sessionId = CommonUtil.generateGUID();
                int now = (int) (System.currentTimeMillis() / 1000);
                boolean isFirstSession = getSessionInfo() == null ? true : false;
                SessionInfo sessionInfo = new SessionInfo(sessionId, now, null,isFirstSession);
                DbStorageService.saveSetting(context, DbSettingKey.SessionInfo, sessionInfo.toString());

                // Report New Session To Server.
                ApiPayloads.NewSessionPayload registerDevicePayload = new ApiPayloads.NewSessionPayload();
                registerDevicePayload.deviceId = DeviceService.getDeviceId(context);
                registerDevicePayload.deviceInfo = deviceInfo;
                registerDevicePayload.sessionInfo = this.getSessionInfo();
                registerDevicePayload.screenName = screenName;
                EventQueueService.getInstance(context).addEventToQueue(registerDevicePayload);

            } else {
                Logger.debug("Session is not expired, use existing session..");
            }
        }catch(Exception err) {
            Logger.error("SessionService", "Error while creating session.", err);
            throw err;
        }
    }

    public SessionInfo getSessionInfo() {

        String setting = DbStorageService.getSetting(context, DbSettingKey.SessionInfo);
        if(setting == null) {
            return null;
        }

        // Parse session Info
        SessionInfo sessionInfo = SessionInfo.fromJson(setting);
        return sessionInfo;
    }
}
