package com.growlytics.android.sdk;

import org.json.JSONException;
import org.json.JSONObject;

final class TrafficSourceInfo {

    private String referrer;
    private JSONObject utmInfo;

    TrafficSourceInfo(String referrer, JSONObject utmInfo){
        this.referrer = referrer;
        this.utmInfo = utmInfo;
    }

    public String getReferrer() {
        return referrer;
    }

    public JSONObject getUtmInfo() {
        return utmInfo;
    }
}
