package com.growlytics.android.sdk;

import android.net.Uri;
import android.net.UrlQuerySanitizer;

import org.json.JSONObject;

final class UriUtil {

    public static JSONObject parseUtmInfo(Uri uri) {

        JSONObject utmInfo = new JSONObject();
        try {

            UrlQuerySanitizer sanitizer = new UrlQuerySanitizer();
            sanitizer.setAllowUnregisteredParamaters(true);
            sanitizer.parseUrl(uri.toString());

            // Don't care for null values - they won't be added anyway
            String source = readQueryStringValue("utm_source", sanitizer);
            String medium = readQueryStringValue("utm_medium", sanitizer);
            String campaign = readQueryStringValue("utm_campaign", sanitizer);

            utmInfo.put("us", source);
            utmInfo.put("um", medium);
            utmInfo.put("uc", campaign);

            Logger.info("Utm data: " + utmInfo.toString(4));
        } catch (Throwable t) {
            Logger.error("UriParser", "Failed to parse deeplink", t);
            // Ignore
        }

        return utmInfo;
    }

    private static String readQueryStringValue(String key, UrlQuerySanitizer sanitizer) {
        if (key == null || sanitizer == null) return null;
        try {
            String value = sanitizer.getValue(key);

            if (value == null) return null;
            if (value.length() > Constants.MAX_VALUE_LENGTH)
                return value.substring(0, Constants.MAX_VALUE_LENGTH);
            else
                return value;
        } catch (Throwable t) {
            Logger.error("UriParser", "Failed to read query value.", t);
            return null;
        }
    }

}
