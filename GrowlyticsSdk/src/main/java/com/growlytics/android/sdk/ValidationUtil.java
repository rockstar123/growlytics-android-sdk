package com.growlytics.android.sdk;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

class ValidationUtil {

    private static final String[] restrictedEventNames = {"GrwNotification Clicked",
            "GrwNotification Viewed", "UTM Visited", "GrwNotification Sent", "App Launched",
            "App Uninstalled", "GrwNotification Bounced"};

    static String validateEventAttributes(Map<String, Object> eventAttributes) {

        ArrayList<String> errors = new ArrayList<String>();
        for (String key : eventAttributes.keySet()) {

            // Validate attribute key.
            String result = _validateAttributeKey(key);
            if (result != null) {
                errors.add("Invalid attribute key, " + result);
            }

            // Validate attribute value
            Object value = eventAttributes.get(key);
            if (value == null) {
                continue;
            }

            // If it's any type of number, send it back
            if (value instanceof Integer
                    || value instanceof Float
                    || value instanceof Boolean
                    || value instanceof Double
                    || value instanceof Date
                    || value instanceof Long) {
                continue;
            } else if (value instanceof String) {
                String stringValue = (String) value;
                if (stringValue.length() > Constants.MAX_VALUE_LENGTH) {
                    errors.add("Invalid attribute value, attribute string value can not exceed " + Constants.MAX_VALUE_LENGTH + " characters.");
                }
            } else {
                errors.add("Attribute value can only be of type: String, Boolean, Long, Integer, Float, Double, or Date.");
            }
        }

        if(errors.size() > 0){
            String error = errors.size() + "attributes are invalid.";
            error += TextUtils.join(" | ", errors);
            return error;
        }

        return null;
    }

    private static String _validateAttributeKey(String attributeKey) {

        // Check if its empty
        if (attributeKey == null || attributeKey.trim().equals("")) {
            return "Attribute key can not be empty or null.";
        }

        // Check if it exceeds allowed characters
        if (attributeKey.length() > Constants.MAX_KEY_LENGTH) {
            return "Attribute key can not be greater than " + Constants.MAX_KEY_LENGTH + " characters.";
        }

        return null;
    }

    static String validateEventName(String eventName) {

        // Check if its empty
        if (eventName == null || eventName.trim().equals("")) {
            return "EVent name can not be empty or null.";
        }

        // Check if it exceeds allowed characters
        if (eventName.length() > Constants.MAX_KEY_LENGTH) {
            return "EVent name can not be greater than " + Constants.MAX_KEY_LENGTH + " characters.";
        }

        // Reserved event name are not allowed.
        for (String x : restrictedEventNames)
            if (eventName.equalsIgnoreCase(x)) {
                return eventName + " is a restricted event name. Please read docs at docs.growlytics.in to see all restricted events.";
            }
        return null;
    }

    static String validateCustomerId(String customerId){

        // Null not allowed
        if (customerId == null) {
            return "Customer Id can not be null.";
        }

        // Can not exceed 40 characters
        if (customerId.length() > Constants.MAX_CUSTOMER_ID_LENGTH) {
            return "Customer Id can not be greater than " + Constants.MAX_CUSTOMER_ID_LENGTH + " characters.";
        }

        return null;

    }

}
