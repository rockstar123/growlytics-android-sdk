package com.growlytics.testApp;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NavUtils;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.growlytics.android.sdk.Growlytics;

import java.util.HashMap;

/**
 * An activity representing a single Item detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link ItemListActivity}.
 */
public class ItemDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own detail action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putString(ItemDetailFragment.ARG_ITEM_ID,
                    getIntent().getStringExtra(ItemDetailFragment.ARG_ITEM_ID));
            ItemDetailFragment fragment = new ItemDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.item_detail_container, fragment)
                    .commit();
        }

        // App installed referral logic
//        Intent i = new Intent("com.android.vending.INSTALL_REFERRER");
//        //Set Package name
//        i.setPackage("com.growlytics.testApp");
//        //referrer is a composition of the parameter of the campaing
//        i.putExtra("referrer", "utm_source=test_source&sdf=sdjkf");
//        sendBroadcast(i);
//        Log.d("App Install", "Sent broadcast");

        // Track custom event
//        Log.d("Growlytics", "==>Putting dummy event");
//        Map<String, Object> attributes = new HashMap<>();
//        double d1 = 123.4;
//        double d2 = 1.234e2;
//        float f1 = 123.4f;
//        attributes.put("name", "Ravi Patel");
//        attributes.put("birth date", new Date());
//        attributes.put("has_subscribed", true);
//        attributes.put("int_val", 12);
//        attributes.put("long_val", 12);
//        attributes.put("float_val", f1);
//        attributes.put("double1_val", d1);
//        attributes.put("double2_val", d2);
//        attributes.put("double2_val123", d2);
//        Analytics.getInstance(this).track("DummyEvent", attributes);
//        Log.d("Idem Detail", "Tracked custom event");
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("Amount", Double.valueOf("1000"));
        hashMap.put("Brand", "xyz");
        hashMap.put("Category", "Electronics");
        hashMap.put("Sub Category", "ABC");
        Growlytics.getInstance(this).track("Product Viewed", hashMap);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
//            com.growlytics.android.sdk.Analytics.getInstance(this)
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            NavUtils.navigateUpTo(this, new Intent(this, ItemListActivity.class));

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
